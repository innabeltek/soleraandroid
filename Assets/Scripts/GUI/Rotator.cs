using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Rotator : MonoBehaviour, IInputtable
{
    public event Action<string> UpdateEvent;

    public bool HasHeaders, HasBackButton, HasFilters;
    public bool IsSelectable, IsLoadable;



    public void Initialize(Gui gui, InputManager input, Transform parent)
    {
        this.gui = gui;
        this.input = input;

        this.selfTransform = this.transform;

        this.selfTransform.parent = parent;

        this.title = "Uninitialized";
        this.itemList = null;
        this.selectedIndex = -1;

        this.rotatable = true;
        this.itemID = -1;
        this.filterID = -1;
        this.activeFilter = -1;
        this.bucketIndex = this.itemIndex = 0;
        this.hotIndex = this.filterIndex = -1;
        this.thetaVelocity = 0.0f;
        this.armDistance = 0.0f;

        this.spriteData = null;

        this.headerText = this.selfTransform.Find("Headers/Header Underlay/Header").GetComponent<tk2dTextMesh>();
        this.subheaderText = this.selfTransform.Find("Headers/Subheader Underlay/Subheader").GetComponent<tk2dTextMesh>();
        this.backButton = this.selfTransform.Find("Back").gameObject;
        this.filters = null;
        this.filterButtons = null;

        Transform rotator = this.selfTransform.Find("Rotator");
        this.bucketList = new Bucket[8];
        int i = 0;
        foreach (Transform arm in rotator) {
            this.bucketList[i].Arm = arm;
            this.bucketList[i].Item = arm.GetChild(0);
            this.bucketList[i].ItemSprite = arm.GetChild(0).GetComponent<tk2dSprite>();
            this.bucketList[i].SelectedUnderlay = arm.Find("Sprite/Selected Underlay");
            this.bucketList[i].SubheaderUnderlay = arm.Find("Sprite/Subheader Underlay");
            this.bucketList[i].Subheader = arm.Find("Sprite/Subheader Underlay/Subheader");
            this.bucketList[i].SubheaderText = arm.Find("Sprite/Subheader Underlay/Subheader").GetComponent<tk2dTextMesh>();
            this.bucketList[i].InfoButton = arm.Find("Sprite/Selected Underlay/Info Button");
            ++i;
        }
    }

    public void Preload(string title, ItemList[] itemList, string activeItem)
    {
        if (this.title != title) {
            this.load(title, itemList, activeItem);
        }
    }

    public void Activate()
    {
        this.itemID = -1;
        this.filterID = -1;
        this.thetaVelocity = 0.0f;

        this.input.Register(this);
        this.gameObject.SetActive(true);
    }

    public void Activate(string title, ItemList[] itemList, string activeItem)
    {
        this.Preload(title, itemList, activeItem);
		
        this.Activate();
    }

    public void Deactivate()
    {
        this.gameObject.SetActive(false);
        this.input.Deregister(this);
		
        this.filterID = this.itemID = -1;
        this.thetaVelocity = 0.0f;
    }

    public void Update()
    {
        if (this.rotatable && this.itemID == -1 && this.filterID == -1 && this.thetaVelocity != 0.0f) {
            this.rotate(this.thetaVelocity);// * Time.deltaTime);
            this.thetaVelocity *= 0.95f;
            if (this.thetaVelocity > -0.1f && this.thetaVelocity < 0.1f) {
                this.thetaVelocity = 0.0f;
            }
        }
    }

    public void UpdatePresence(ItemList[] itemList)
    {
        if (this.IsLoadable && this.itemList != null && itemList != null && itemList.Length > 0) {
            this.itemList = itemList;

            ItemList items = this.itemList[this.activeFilter];
            for (int i = 0; i < this.bucketList.Length; ++i) {
                this.bucketList[i].ItemSprite.color = new Color(1.0f, 1.0f, 1.0f, (items.Items[this.getItemID(i)].IsPresent ? 1.0f : 0.5f));
            }
        }
    }

    void load(string title, ItemList[] itemList, string activeItem)
    {
        this.title = title;
        this.itemList = itemList;
        this.selectedIndex = this.itemList[0].Find(activeItem);

        this.rotatable = this.itemList[0].Length > 4;
        this.filterID = this.itemID = -1;
        this.activeFilter = 0;
        this.bucketIndex = 0;
        this.itemIndex = 0;
        this.thetaVelocity = 0.0f;
        this.armDistance = (this.HasFilters ? 10.24f : 9.76f);

        //Debug.Log("Prefabs/GUI/" + this.gui.RetinaString + "/Sprites/" + this.title);
        this.spriteData = (tk2dSpriteCollectionData)Resources.Load("Prefabs/GUI/" + this.gui.RetinaString + "/Sprites/" + this.title, typeof(tk2dSpriteCollectionData));

        this.headerText.text = this.title;
        if (this.HasHeaders && this.itemList[this.activeFilter].Breakpoints.Length > 0) {
            this.subheaderText.transform.parent.gameObject.SetActive(true);
            this.subheaderText.text = this.itemList[this.activeFilter].GetTitle(0, -1);
        } else {
            this.subheaderText.transform.parent.gameObject.SetActive(false);
        }

        if (this.HasBackButton) {
            //Debug.Log(this.spriteData);
            this.backButton.GetComponent<tk2dSprite>().SetSprite(this.spriteData, "Back");
        }

        if (this.filters != null) {
            Destroy(this.filters);
            this.filters = null;
            this.filterButtons = null;
        }
        if (this.HasFilters) {
            this.filters = (GameObject)Instantiate(Resources.Load("Prefabs/GUI/" + this.gui.RetinaString + "/Filters/" + this.title), Vector3.zero, Quaternion.identity);
            Transform filtersTransform = this.filters.transform;
            filtersTransform.parent = this.selfTransform;
            filtersTransform.localPosition = new Vector3(0.0f, 0.0f, -5.0f);
            filtersTransform.localRotation = Quaternion.identity;

            if (this.itemList.Length > 1) {
                this.filterButtons = new GameObject[this.itemList.Length][];
                for (int i = 0; i < this.filterButtons.Length; i++) {
                    this.filterButtons[i] = new GameObject[2];
                    this.filterButtons[i][0] = filtersTransform.Find("Filter 0" + (i + 1) + " Unselected").gameObject;
                    this.filterButtons[i][1] = filtersTransform.Find("Filter 0" + (i + 1) + " Selected").gameObject;
                    this.filterButtons[i][0].SetActive(i != this.activeFilter);
                    this.filterButtons[i][1].SetActive(i == this.activeFilter);
                }
            } else {
                this.filterButtons = null;
            }
        } else {
            this.filters = null;
            this.filterButtons = null;
        }

        this.loadItems();

        Resources.UnloadUnusedAssets();
    }

    void loadItems()
    {
        ItemList items = this.itemList[this.activeFilter];

        this.bucketIndex = 0;
        this.itemIndex = 0;
        this.thetaVelocity = 0.0f;

        this.subheaderText.transform.parent.gameObject.SetActive((this.HasHeaders && items.Breakpoints.Length > 0));
        if (this.HasHeaders && items.Breakpoints.Length > 0) {
            this.subheaderText.text = items.GetTitle(0, -1);
        }

        int si = 0, theta = 45;
        for (int i = 0; i < this.bucketList.Length; ++i) {
            Bucket bucket = this.bucketList[i];

            if (!this.rotatable && i >= items.Length) {
                bucket.Item.gameObject.SetActive(false);
            } else {
                bucket.Item.gameObject.SetActive(true);

                bucket.Arm.localEulerAngles = new Vector3(0.0f, 0.0f, (float)theta);
                bucket.Item.localEulerAngles = new Vector3(0.0f, 0.0f, -1.0f * (float)theta);
                bucket.Item.localPosition = new Vector3(this.armDistance, 0.0f, 0.0f);

                if (this.HasHeaders && items.Breakpoints.Length > 1 && items.IsBreakpoint(si)) {
                    bucket.SubheaderUnderlay.gameObject.SetActive(bucket.SubheaderUnderlay.position.y <= 5.58);
                    bucket.Subheader.gameObject.SetActive(true);
                    bucket.SubheaderText.text = items.GetTitle(si);
                } else {
                    bucket.SubheaderUnderlay.gameObject.SetActive(false);
                    bucket.Subheader.gameObject.SetActive(false);
                }
                if (this.IsSelectable) {
                    //Debug.Log("si: " + si + ", selectedIndex: " + this.selectedIndex);
                    bucket.SelectedUnderlay.gameObject.SetActive(si == this.selectedIndex);
                }

                bucket.ItemSprite.SetSprite(this.spriteData, items.Items[si].Name);
                if (this.IsLoadable) {
                    bucket.ItemSprite.color = new Color(1.0f, 1.0f, 1.0f, (items.Items[si].IsPresent ? 1.0f : 0.5f));
                }

                theta = (theta - 15).Wrap(360);
                si = (si + 1).Wrap(items.Length);
            }
        }

        if (this.HasFilters && this.filterButtons != null) {
            for (int i = 0; i < this.filterButtons.Length; ++i) {
                this.filterButtons[i][0].SetActive(i != this.activeFilter);
                this.filterButtons[i][1].SetActive(i == this.activeFilter);
            }
        }

        if (this.itemList[this.activeFilter].Length > 0) {
            this.rotate(Mathf.Rad2Deg * Mathf.Asin((this.HasHeaders ? 4.29f : 4.97f) / this.armDistance) - 45.01f);
        }
    }

    void rotate(float theta)
    {
        if (theta > 15.0f || theta < -15.0f) {
            while (theta > 15.0f) {
                this.rotate(15.0f);
                theta -= 15.0f;
            }
            while (theta < -15.0f) {
                this.rotate(-15.0f);
                theta += 15.0f;
            }
        }

        ItemList items = this.itemList[this.activeFilter];
        float dTheta = 0.0f, z = 0.0f;
        for (int i = 0; i < this.bucketList.Length; ++i) {
            Bucket bucket = this.bucketList[i];
            z = bucket.Arm.eulerAngles.z + theta;
            if (z <= 60.0f || z >= 300.0f) {
                dTheta = 0;
            } else if (z > 60.0f && z < 180.0f) {
                dTheta = 240.0f;
                this.itemIndex = (this.itemIndex + 1).Wrap(items.Length);
                this.bucketIndex = (this.bucketIndex + 1).Wrap(this.bucketList.Length);
            } else if (z < 300.0f && z > 180.0f) {
                dTheta = -240.0f;
                this.itemIndex = (this.itemIndex - 1).Wrap(items.Length);
                this.bucketIndex = (this.bucketIndex - 1).Wrap(this.bucketList.Length);
            }
            bucket.Arm.Rotate(0.0f, 0.0f, theta + dTheta);
            bucket.Item.Rotate(0.0f, 0.0f, -theta - dTheta);

            if (dTheta != 0.0f) {
                int newIndex = this.itemIndex;
                if (dTheta > 0.0f) {
                    newIndex = (newIndex + 7).Wrap(items.Length);
                }

                if (this.HasHeaders) {
                    if (items.Breakpoints.Length > 1 && items.IsBreakpoint(newIndex)) {
                        bucket.SubheaderUnderlay.gameObject.SetActive(bucket.SubheaderUnderlay.position.y <= 5.58);
                        bucket.Subheader.gameObject.SetActive(true);
                        bucket.SubheaderText.text = items.GetTitle(newIndex);
                    } else {
                        bucket.SubheaderUnderlay.gameObject.SetActive(false);
                        bucket.Subheader.gameObject.SetActive(false);
                    }
                }

                if (this.IsSelectable) {
                    bucket.SelectedUnderlay.gameObject.SetActive(newIndex == this.selectedIndex);
                }

                bucket.ItemSprite.SetSprite(this.spriteData, items.Items[newIndex].Name);
                if (this.IsLoadable) {
                    bucket.ItemSprite.color = new Color(1.0f, 1.0f, 1.0f, (items.Items[newIndex].IsPresent ? 1.0f : 0.5f));
                }
            }

            if (this.HasHeaders) {
                if (bucket.Subheader.gameObject.activeSelf) {
                    if (bucket.SubheaderUnderlay.position.y <= 5.6 && !bucket.SubheaderUnderlay.gameObject.activeSelf) {
                        bucket.SubheaderUnderlay.gameObject.SetActive(true);
                        this.subheaderText.text = items.GetTitle(getItemID(i), -1);
                    } else if (bucket.SubheaderUnderlay.position.y > 5.6 && bucket.SubheaderUnderlay.gameObject.activeSelf) {
                        bucket.SubheaderUnderlay.gameObject.SetActive(false);
                        this.subheaderText.text = items.GetTitle(getItemID(i));
                    }
                }

                if (bucket.SubheaderUnderlay.gameObject.activeSelf && bucket.Subheader.gameObject.activeSelf) {
                    float x = (this.gui.Width / -200.0f) - 7.2f + 11.2f * Mathf.Cos(Mathf.Asin(bucket.Subheader.position.y / 11.2f));
                    bucket.Subheader.Translate(x - bucket.Subheader.position.x, 0.0f, 0.0f);
                }
            }

            if (this.IsSelectable && this.selectedIndex == getItemID(i)) {
                float x = (this.gui.Width / -200.0f) - 7.2f + 11.2f * Mathf.Cos(Mathf.Asin(bucket.InfoButton.position.y / 11.2f));
                bucket.InfoButton.Translate(x - bucket.InfoButton.position.x, 0.0f, 0.0f);
            }
        }
    }

    void filterClicked(int index)
    {
        if (this.activeFilter != index && index >= 0) {
            this.selectedIndex = this.itemList[index].Find((this.selectedIndex >= 0 ? this.itemList[this.activeFilter].Items[this.selectedIndex].Name : ""));
            this.activeFilter = index;
            this.loadItems();
        }
    }

    int getItemID(int i)
    {
        return ((i - this.bucketIndex).Wrap(this.bucketList.Length) + this.itemIndex).Wrap(this.itemList[this.activeFilter].Length);
    }




    Gui gui;
    InputManager input;

    Transform selfTransform;

    string title;
    ItemList[] itemList;
    int selectedIndex;

    bool rotatable;
    int itemID, filterID;
    int hotIndex, filterIndex;
    int activeFilter;
    int bucketIndex, itemIndex;
    float thetaVelocity, armDistance;

    tk2dSpriteCollectionData spriteData;
    tk2dTextMesh headerText, subheaderText;
    GameObject backButton, filters;
    GameObject[][] filterButtons;
    Bucket[] bucketList;



#region INPUT
    public bool OnTouchStart(Touch touch)
    {
        
        float x = touch.position.x * this.gui.RetinaConstant + 720.0f, y = touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f;
        float x2 = Mathf.Pow(x, 2.0f), y2 = Mathf.Pow(y, 2.0f);

        if (x2 + y2 < 1290496.0f) { // Mathf.Pow(1136.0f, 2.0f) )
            if (this.itemID == -1 && this.filterID == -1) {
                this.thetaVelocity = 0.0f;
                if (x2 + y2 < 665856.0f) { // Mathf.Pow(816.0f, 2.0f) )
                    if (this.HasBackButton) {
                        this.filterID = touch.fingerId;
                        this.filterIndex = 1000;
                    }
                } else if (this.HasFilters && x2 + y2 < 831744.0f) { // Mathf.Pow(912.0f, 2.0f) )
                    if (this.itemList.Length == 3) {
                        if (y > 176.0f) {
                            this.filterIndex = 1001;
                        } else if (y > -176.0f) {
                            this.filterIndex = 1002;
                        } else {
                            this.filterIndex = 1003;
                        }
                        this.filterID = touch.fingerId;
                    } else if (this.itemList.Length == 2) {
                        if (y > 0.0f) {
                            this.filterIndex = 1001;
                        } else {
                            this.filterIndex = 1002;
                        }
                        this.filterID = touch.fingerId;
                    }
                } else {
                    float buttonRadius = (this.HasFilters ? 65.0f : 80.0f);
                    for (int i = 0; i < (this.rotatable ? this.bucketList.Length : this.itemList[this.activeFilter].Length); i++) {
                        float bucketY = this.bucketList[i].Item.position.y * 100.0f;
                        if (y < bucketY + buttonRadius && y > bucketY - buttonRadius) {
                            this.hotIndex = getItemID(i);
                        }
                    }
                    this.itemID = touch.fingerId;
                }
            }
			
            return true;
        }
		
        return false;
    }

    public void OnTouchMove(Touch touch)
    {
        if (touch.fingerId == this.itemID) {
            this.hotIndex = -1;
        } else if (touch.fingerId == this.filterID) {
            //this.filterIndex = -1;
        }
		
        if (this.rotatable && this.itemID == touch.fingerId) {
#if UNITY_IPHONE
            float deltaY = Mathf.Clamp(touch.deltaPosition.y * this.gui.RetinaConstant / (this.armDistance * 100.0f), -1.0f, 1.0f);
#elif UNITY_ANDROID || UNITY_EDITOR
			float deltaY = Mathf.Clamp(touch.deltaPosition.y * (Time.deltaTime / touch.deltaTime) * this.gui.RetinaConstant / (this.armDistance * 100.0f), -1.0f, 1.0f);
#endif
            float theta = Mathf.Clamp(Mathf.Rad2Deg * Mathf.Asin(deltaY), -15.0f, 15.0f);
            this.thetaVelocity = Mathf.Clamp(0.3f * this.thetaVelocity + 0.7f * theta, -200.0f, 200.0f);
            if (this.thetaVelocity == Mathf.Infinity)
                this.thetaVelocity = 0.0f;
            this.rotate(this.thetaVelocity);
        }
    }

    public void OnTouchCancel(int fingerid)
    {
        if (fingerid == this.itemID) {
            this.itemID = -1;
            this.hotIndex = -1;
        } else if (fingerid == this.filterID) {
            this.filterID = -1;
            this.filterIndex = -1;
        }
    }

    public void OnTouchEnd(Touch touch)
    {
        if (touch.fingerId == this.itemID) {
            this.itemID = -1;
            if (this.hotIndex > -1) {
                this.selectedIndex = this.hotIndex;
                UpdateEvent(this.itemList[this.activeFilter].Items[this.selectedIndex].Name);
                if (this.IsSelectable) {
                    for (int i = 0; i < (this.rotatable ? this.bucketList.Length : this.itemList[this.activeFilter].Length); i++) {
                        if (this.hotIndex == getItemID(i)) {
                            Bucket bucket = this.bucketList[i];
                            bucket.SelectedUnderlay.gameObject.SetActive(true);
                            float x = (this.gui.Width / -200.0f) - 7.2f + 11.2f * Mathf.Cos(Mathf.Asin((bucket.InfoButton.position.y) / 11.2f));
                            bucket.InfoButton.Translate(x - bucket.InfoButton.position.x, 0.0f, 0.0f);
                        } else {
                            this.bucketList[i].SelectedUnderlay.gameObject.SetActive(false);
                        }
                    }
                }
            }
        } else if (touch.fingerId == this.filterID) {
            this.filterID = -1;

            float x = touch.position.x * this.gui.RetinaConstant + 720.0f, y = touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f;
            float x2 = Mathf.Pow(x, 2.0f), y2 = Mathf.Pow(y, 2.0f);

            if (x2 + y2 < 1290496.0f) {
                if (x2 + y2 < 665856.0f) {
                    if (this.filterIndex == 1000) {
                        this.UpdateEvent("BACK");
                    }
                } else if (x2 + y2 < 831744.0f) {
                    if (this.itemList.Length == 3) {
                        if (this.filterIndex == 1001 &&
                            y > 176.0f) {
                            this.filterClicked(0);
                        } else if (this.filterIndex == 1002 &&
                                   y > -176.0f) {
                            this.filterClicked(1);
                        } else if (this.filterIndex == 1003 &&
                                   y <= -176.0f) {
                            this.filterClicked(2);
                        }
                    } else if (this.itemList.Length == 2) {
                        if (this.filterIndex == 1001 &&
                            y > 0.0f) {
                            this.filterClicked(0);
                        } else if (this.filterIndex == 1002 &&
                                   y <= 0.0f) {
                            this.filterClicked(1);
                        }
                    }
                }
            }
        }
    }
#endregion
}
