using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class SinkInfoOverlay : Overlay
{
    public event Action DownloadClickedEvent, DeleteClickedEvent, StoreClickedEvent;



    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.title = this.baseTransform.Find("Content/Sink Title").GetComponent<tk2dTextMesh>();
        this.description = this.baseTransform.Find("Content/Sink Body").GetComponent<tk2dTextMesh>();
		
        this.image = this.baseTransform.Find("Content/Sink Image/default").GetComponent<MeshRenderer>();
		
        this.deleteButton = this.baseTransform.Find("Content/Delete Button");
        this.downloadButton = this.baseTransform.Find("Content/Download Button");
        this.storeButton = this.baseTransform.Find("Content/Store Button");
        this.closeButton = this.baseTransform.Find("Content/Close Button");
    }

    public void Activate(SinkInfo sinkInfo, bool present)
    {
        base.Activate();

        this.Refresh(sinkInfo, present);

        Resources.UnloadUnusedAssets();

        this.baseTransform.gameObject.SetActive(true);
    }

    public void Refresh(SinkInfo sinkInfo, bool present)
    {
        this.title.text = sinkInfo.Title;
        this.description.text = sinkInfo.Description;
        this.image.material.mainTexture = sinkInfo.InfoTexture;
        this.deleteButton.gameObject.SetActive(present);
        this.downloadButton.gameObject.SetActive(!present);
        this.storeButton.gameObject.SetActive(true);
    }


    public void DownloadFinished()
    {
        this.SetClicksLocked(false);
    }

    public void DeleteFinished()
    {
        this.SetClicksLocked(false);
    }

    public void StoreFinished()
    {
        this.SetClicksLocked(false);
    }



    void downloadClicked()
    {
        this.SetClicksLocked(true);

        if (this.DownloadClickedEvent != null) {
            this.DownloadClickedEvent();
        }
    }

    void deleteClicked()
    {
        this.SetClicksLocked(true);

        if (this.DeleteClickedEvent != null) {
            this.DeleteClickedEvent();
        }
    }

    void storeClicked()
    {
        this.SetClicksLocked(true);

        if (this.StoreClickedEvent != null) {
            this.StoreClickedEvent();
        }
    }



    Transform deleteButton, downloadButton, storeButton, closeButton;
    tk2dTextMesh title, description;
    MeshRenderer image;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1 && !this.clicksLocked) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;
			
            if (x > this.storeButton.position.x && x < this.storeButton.position.x + 1.6f &&
                y > this.storeButton.position.y && y < this.storeButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (this.deleteButton.gameObject.activeSelf &&
                       x > this.deleteButton.position.x && x < this.deleteButton.position.x + 2.6f &&
                       y > this.deleteButton.position.y && y < this.deleteButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            } else if (this.downloadButton.gameObject.activeSelf &&
                       x > this.downloadButton.position.x && x < this.downloadButton.position.x + 3.0f &&
                       y > this.downloadButton.position.y && y < this.downloadButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 2;
            } else if ((x > this.closeButton.position.x - 0.45f && x < this.closeButton.position.x + 0.15f &&
                       y > this.closeButton.position.y - 0.45f && y < this.closeButton.position.y + 0.15f) ||
                       (x < transform.position.x - 6.0f || x > transform.position.x + 6.0f ||
                       y < transform.position.y - 2.8f || y > transform.position.y + 2.8f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.storeButton.position, 0.6f, 1.6f, 0.0f, 0.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (this.deleteButton.gameObject.activeSelf &&
                       this.areCoordsInsideRect(coords, this.deleteButton.position, 0.6f, 2.6f, 0.0f, 0.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            } else if (this.downloadButton.gameObject.activeSelf &&
                       this.areCoordsInsideRect(coords, this.downloadButton.position, 0.6f, 3.0f, 0.0f, 0.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 2;
            } else if (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                       this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.8f, 6.0f, 2.8f, 6.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }
		
        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        if (!this.clicksLocked) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

            if (this.hotIndex == 0 &&
                x > this.storeButton.position.x && x < this.storeButton.position.x + 1.6f &&
                y > this.storeButton.position.y && y < this.storeButton.position.y + 0.6f) {
                StoreClicked();
            } else if (this.hotIndex == 1 &&
                       x > this.deleteButton.position.x && x < this.deleteButton.position.x + 2.6f &&
                       y > this.deleteButton.position.y && y < this.deleteButton.position.y + 0.6f) {
                DeleteClicked();
            } else if (this.hotIndex == 2 &&
                       x > this.downloadButton.position.x && x < this.downloadButton.position.x + 3.0f &&
                       y > this.downloadButton.position.y && y < this.downloadButton.position.y + 0.6f) {
                DownloadClicked();
            } else if (this.hotIndex == 100 &&
                       ((x > this.closeButton.position.x - 0.45f && x < this.closeButton.position.x + 0.15f &&
                       y > this.closeButton.position.y - 0.45f && y < this.closeButton.position.y + 0.15f) ||
                       (x < transform.position.x - 6.0f || x > transform.position.x + 6.0f ||
                       y < transform.position.y - 2.8f || y > transform.position.y + 2.8f))) {
                Deactivate();
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.hotIndex == 0 &&
                this.areCoordsInsideRect(coords, this.storeButton.position, 0.6f, 1.6f, 0.0f, 0.0f)) {
                this.storeClicked();
            } else if (this.hotIndex == 1 &&
                       this.deleteButton.gameObject.activeSelf &&
                       this.areCoordsInsideRect(coords, this.deleteButton.position, 0.6f, 2.6f, 0.0f, 0.0f)) {
                this.deleteClicked();
            } else if (this.hotIndex == 2 &&
                       this.downloadButton.gameObject.activeSelf &&
                       this.areCoordsInsideRect(coords, this.downloadButton.position, 0.6f, 3.0f, 0.0f, 0.0f)) {
                this.downloadClicked();
            } else if (this.hotIndex == 100 &&
                       this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                       this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.8f, 6.0f, 2.8f, 6.0f)) {
                this.Deactivate();
            }
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
