﻿using UnityEngine;
using System;
using System.Collections;

public class Overlay : MonoBehaviour, IInputtable
{
    public event Action StartEvent, EndEvent;



    public virtual void Deactivate()
    {
        this.gui.UpdateReticle(this.reticleState);
		
        this.input.Deregister(this);

        this.DeactivateEvents();

        this.gameObject.SetActive(false);
        Destroy(this.gameObject);
        Resources.UnloadUnusedAssets();
    }



    protected void Initialize(Gui gui, InputManager input)
    {
        this.gui = gui;

        this.reticleState = false;
        this.selfTransform = this.transform;
        this.baseTransform = this.selfTransform.Find("Base");


        this.input = input;
		
        this.clicksLocked = false;
        this.touchID = this.hotIndex = -1;
    }

    protected void Activate(bool showunderlay = true)
    {
        this.baseTransform.Translate(new Vector3((this.gui.LeftRingActive ? 0.96f : 0.0f), 0.0f, 0.0f));

        if (!showunderlay) {
            this.baseTransform.Find("Frame/Underlay").gameObject.SetActive(false);
        }

        if (this.StartEvent != null) {
            this.StartEvent();
        }

        this.input.Register(this);

        this.reticleState = this.gui.ReticleActive;
        this.gui.UpdateReticle(false);
    }

    protected void SetClicksLocked(bool state)
    {
        this.clicksLocked = state;
        if (this.clicksLocked) {
            this.touchID = this.hotIndex = -1;
        }
    }

    protected void DeactivateEvents()
    {
        if (EndEvent != null) {
            EndEvent();
        }
		
        StartEvent = null;
        EndEvent = null;
    }



    protected Gui gui;

    protected bool reticleState;
    protected Transform selfTransform, baseTransform;



#region INPUT
    public virtual bool OnTouchStart(Touch touch)
    {
        return true;
    }

    public virtual void OnTouchMove(Touch touch)
    {
    }

    public virtual void OnTouchCancel(int fingerid)
    {
    }

    public virtual void OnTouchEnd(Touch touch)
    {
    }



    protected Vector2 calculateCoordinates(Vector2 coords)
    {
        return new Vector2(
            (coords.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f,
            (coords.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f
        );
    }

    protected bool areCoordsInsideRect(Vector2 coords, Vector2 rect, float top, float right, float bottom, float left)
    {
        return (
            coords.x > rect.x - left &&
            coords.x < rect.x + right &&
            coords.y > rect.y - bottom &&
            coords.y < rect.y + top
        );
    }

    protected bool areCoordsOutsideRect(Vector2 coords, Vector2 rect, float top, float right, float bottom, float left)
    {
        return (
            coords.x < rect.x - left &&
            coords.x > rect.x + right &&
            coords.y < rect.y - bottom &&
            coords.y > rect.y + top
        );
    }



    protected InputManager input;

    protected bool clicksLocked;
    protected int touchID, hotIndex;
#endregion
}