using UnityEngine;
using System.Collections;

public class CountertopInfoOverlay : Overlay
{
    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.closeButton = this.baseTransform.Find("Content/Close Button");
    }

    public void Activate(string countertopName, CountertopInfo countertopInfo)
    {
        base.Activate();

        tk2dTextMesh title = this.baseTransform.Find("Content/Countertop Title").GetComponent<tk2dTextMesh>();
        tk2dTextMesh subtitle = this.baseTransform.Find("Content/Countertop Subtitle").GetComponent<tk2dTextMesh>();
        tk2dTextMesh description = this.baseTransform.Find("Content/Countertop Body").GetComponent<tk2dTextMesh>();
        MeshRenderer image = this.baseTransform.Find("Content/Countertop Image/default").GetComponent<MeshRenderer>();

        Texture2D tex = (Texture2D)Resources.Load("Textures/" + this.gui.RetinaString + "/Countertops Info/" + countertopInfo.Category + "/" + countertopName);
        if (tex != null) {
            image.material.mainTexture = tex;
        }

        title.text = countertopName;
        subtitle.text = countertopInfo.Category;
        description.text = countertopInfo.Description;

        this.baseTransform.gameObject.SetActive(true);
    }



    Transform closeButton;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1 && !this.clicksLocked) {
            Vector2 coords = this.calculateCoordinates(touch.position);

            /*
            if ((x > this.closeButton.position.x - 0.45f && x < this.closeButton.position.x + 0.15f &&
                 y > this.closeButton.position.y - 0.45f && y < this.closeButton.position.y + 0.15f) ||
                (x < this.selfTransform.position.x - 6.0f || x > this.selfTransform.position.x + 6.0f ||
                 y < this.selfTransform.position.y - 2.0f || y > this.selfTransform.position.y + 2.0f)) {
            */
            if (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.0f, 6.0f, 2.0f, 6.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }

        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = this.calculateXCoordinate(touch.position.x), y = this.calculateYCoordinate(touch.position.y);

        if (this.hotIndex == 100 &&
            (x > this.closeButton.position.x - 0.45f && x < this.closeButton.position.x + 0.15f &&
             y > this.closeButton.position.y - 0.45f && y < this.closeButton.position.y + 0.15f) ||
            (x < this.selfTransform.position.x - 6.0f || x > this.selfTransform.position.x + 6.0f ||
             y < this.selfTransform.position.y - 2.0f || y > this.selfTransform.position.y + 2.0f)) {
            Deactivate();
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
            this.areCoordsOutsideRect(coords, this.selfTransform.position, 6.0f, 2.0f, 6.0f, 2.0f)) {
            this.Deactivate();
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}