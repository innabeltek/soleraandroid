﻿using UnityEngine;
using System;
using System.Collections;

public class MenuOverlay : Overlay
{
    public event Action TutorialEvent, MarkerEvent, WebsiteEvent, DownloadAllEvent, DeleteAllEvent;



    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.closeButton = this.baseTransform.Find("Content/Close Button");
        this.tutorialButton = this.baseTransform.Find("Content/Guide");
        this.editButton = this.baseTransform.Find("Content/Edit Sinks");
        this.markerButton = this.baseTransform.Find("Content/Print Target");
        this.websiteButton = this.baseTransform.Find("Content/Website");
		
        this.baseTransformTwo = this.transform.Find("Base2");
        this.backButton = this.baseTransformTwo.Find("Content/Back Button");
        this.downloadAllButton = this.baseTransformTwo.Find("Content/Download All");
        this.deleteAllButton = this.baseTransformTwo.Find("Content/Delete All");
    }

    public void Activate()
    {
        base.Activate();

        this.isFirstState = true;

        this.baseTransform.gameObject.SetActive(true);
    }

    public void TutorialFinished()
    {
        this.gameObject.SetActive(true);

        this.SetClicksLocked(false);
    }

    public void MarkerFinished()
    {
        this.SetClicksLocked(false);
    }

    public void WebsiteFinished()
    {
        this.SetClicksLocked(false);
    }

    public void DownloadAllFinished()
    {
        this.SetClicksLocked(false);
    }

    public void DeleteAllFinished()
    {
        this.SetClicksLocked(false);
    }



    void tutorialClicked()
    {
        this.SetClicksLocked(true);

        this.gameObject.SetActive(false);

        if (this.TutorialEvent != null) {
            this.TutorialEvent();
        }
    }

    void editClicked()
    {
        this.isFirstState = false;
        this.baseTransform.gameObject.SetActive(false);
        this.baseTransformTwo.gameObject.SetActive(true);
    }

    void markerClicked()
    {
        this.SetClicksLocked(true);

        if (this.MarkerEvent != null) {
            this.MarkerEvent();
        }
    }

    void websiteClicked()
    {
        this.SetClicksLocked(true);

        if (this.WebsiteEvent != null) {
            this.WebsiteEvent();
        }
    }

    void downloadAllClicked()
    {
        this.SetClicksLocked(true);

        if (this.DownloadAllEvent != null) {
            this.DownloadAllEvent();
        }
    }

    void deleteAllClicked()
    {
        this.SetClicksLocked(true);

        if (this.DeleteAllEvent != null) {
            this.DeleteAllEvent();
        }
    }

    void backClicked()
    {
        this.isFirstState = true;
        this.baseTransform.gameObject.SetActive(true);
        this.baseTransformTwo.gameObject.SetActive(false);
    }



    bool isFirstState;
    Transform closeButton, tutorialButton, editButton, markerButton, websiteButton;
    Transform baseTransformTwo, backButton, downloadAllButton, deleteAllButton;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        /*
        if (this.touchID == -1 && !this.clicksLocked) {
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;
			
            if (this.isFirstState) {
                if (x < this.tutorialButton.position.x + 1.1f && x > this.tutorialButton.position.x - 1.1f &&
                    y < this.tutorialButton.position.y + 1.15f && y > this.tutorialButton.position.y - 1.15f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 0;
                } else if (x < this.editButton.position.x + 1.1f && x > this.editButton.position.x - 1.1f &&
                           y < this.editButton.position.y + 1.15f && y > this.editButton.position.y - 1.15f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 80;
                } else if (x < this.markerButton.position.x + 1.1f && x > this.markerButton.position.x - 1.1f &&
                           y < this.markerButton.position.y + 1.15f && y > this.markerButton.position.y - 1.15f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 1;
                } else if (x < this.websiteButton.position.x + 1.1f && x > this.websiteButton.position.x - 1.1f &&
                           y < this.websiteButton.position.y + 1.15f && y > this.websiteButton.position.y - 1.15f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 2;
                } else if ((x < this.closeButton.position.x + 0.15f && x > this.closeButton.position.x - 0.45f &&
                           y < this.closeButton.position.y + 0.15f && y > this.closeButton.position.y - 0.45f) ||
                           (x < transform.position.x - 5.44 || x > transform.position.x + 5.44 ||
                           y < transform.position.y - 2.24 || y > transform.position.y + 2.24)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 100;
                }
            } else {
                if (x < this.downloadAllButton.position.x + 1.1f && x > this.downloadAllButton.position.x - 1.1f &&
                    y < this.downloadAllButton.position.y + 1.3f && y > this.downloadAllButton.position.y - 1.3f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 3;
                } else if (x < this.deleteAllButton.position.x + 1.1f && x > this.deleteAllButton.position.x - 1.1f &&
                           y < this.deleteAllButton.position.y + 1.3f && y > this.deleteAllButton.position.y - 1.3f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 4;
                } else if (x < this.backButton.position.x + 1.6f && x > this.backButton.position.x &&
                           y < this.backButton.position.y && y > this.backButton.position.y - 0.6f) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 90;
                } else if ((x < this.closeButton.position.x + 0.15f && x > this.closeButton.position.x - 0.45f &&
                           y < this.closeButton.position.y + 0.15f && y > this.closeButton.position.y - 0.45f) ||
                           (x < transform.position.x - 5.44 || x > transform.position.x + 5.44 ||
                           y < transform.position.y - 2.24 || y > transform.position.y + 2.24)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 100;
                }
            }
        }
        */

        if (this.touchID == -1 && !this.clicksLocked) {
            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.isFirstState) {
                if (this.areCoordsInsideRect(coords, this.tutorialButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 0;
                } else if (this.areCoordsInsideRect(coords, this.editButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 80;
                } else if (this.areCoordsInsideRect(coords, this.markerButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 1;
                } else if (this.areCoordsInsideRect(coords, this.websiteButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 2;
                } else if (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                           this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.24f, 5.44f, 2.24f, 5.44f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 100;
                }
            } else {
                if (this.areCoordsInsideRect(coords, this.downloadAllButton.position, 1.3f, 1.1f, 1.3f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 3;
                } else if (this.areCoordsInsideRect(coords, this.deleteAllButton.position, 1.3f, 1.1f, 1.3f, 1.1f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 4;
                } else if (this.areCoordsInsideRect(coords, this.backButton.position, 0.0f, 1.6f, 0.6f, 0.0f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 90;
                } else if (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                           this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.24f, 5.44f, 2.24f, 5.44f)) {
                    this.touchID = touch.fingerId;
                    this.hotIndex = 100;
                }
            }
        }
		
        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

        if (!this.clicksLocked) {
            if (this.isFirstState) {
                if (this.hotIndex == 0 &&
                    x < this.tutorialButton.position.x + 1.1f && x > this.tutorialButton.position.x - 1.1f &&
                    y < this.tutorialButton.position.y + 1.15f && y > this.tutorialButton.position.y - 1.15f) {
                    TutorialClicked();
                } else if (this.hotIndex == 80 &&
                           x < this.editButton.position.x + 1.1f && x > this.editButton.position.x - 1.1f &&
                           y < this.editButton.position.y + 1.15f && y > this.editButton.position.y - 1.15f) {
                    EditClicked();
                } else if (this.hotIndex == 1 &&
                           x < this.markerButton.position.x + 1.1f && x > this.markerButton.position.x - 1.1f &&
                           y < this.markerButton.position.y + 1.15f && y > this.markerButton.position.y - 1.15f) {
                    MarkerClicked();
                } else if (this.hotIndex == 2 &&
                           x < this.websiteButton.position.x + 1.1f && x > this.websiteButton.position.x - 1.1f &&
                           y < this.websiteButton.position.y + 1.15f && y > this.websiteButton.position.y - 1.15f) {
                    WebsiteClicked();
                } else if (this.hotIndex == 100 &&
                           ((x < this.closeButton.position.x + 0.15f && x > this.closeButton.position.x - 0.45f &&
                           y < this.closeButton.position.y + 0.15f && y > this.closeButton.position.y - 0.45f) ||
                           (x < transform.position.x - 5.44 || x > transform.position.x + 5.44 ||
                           y < transform.position.y - 2.24 || y > transform.position.y + 2.24))) {
                    Deactivate();
                }
            } else {
                if (this.hotIndex == 3 &&
                    x < this.downloadAllButton.position.x + 1.1f && x > this.downloadAllButton.position.x - 1.1f &&
                    y < this.downloadAllButton.position.y + 1.3f && y > this.downloadAllButton.position.y - 1.3f) {
                    DownloadAllClicked();
                } else if (this.hotIndex == 4 &&
                           x < this.deleteAllButton.position.x + 1.1f && x > this.deleteAllButton.position.x - 1.1f &&
                           y < this.deleteAllButton.position.y + 1.3f && y > this.deleteAllButton.position.y - 1.3f) {
                    DeleteAllClicked();
                } else if (this.hotIndex == 90 &&
                           x < this.backButton.position.x + 1.6f && x > this.backButton.position.x &&
                           y < this.backButton.position.y && y > this.backButton.position.y - 0.6f) {
                    BackClicked();
                } else if (this.hotIndex == 100 &&
                           ((x < this.closeButton.position.x + 0.15f && x > this.closeButton.position.x - 0.45f &&
                           y < this.closeButton.position.y + 0.15f && y > this.closeButton.position.y - 0.45f) ||
                           (x < transform.position.x - 5.44 || x > transform.position.x + 5.44 ||
                           y < transform.position.y - 2.24 || y > transform.position.y + 2.24))) {
                    Deactivate();
                }
            }
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (!this.clicksLocked) {
            if (this.isFirstState) {
                if (this.hotIndex == 0 &&
                    this.areCoordsInsideRect(coords, this.tutorialButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.tutorialClicked();
                } else if (this.hotIndex == 80 &&
                           this.areCoordsInsideRect(coords, this.editButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.editClicked();
                } else if (this.hotIndex == 1 &&
                           this.areCoordsInsideRect(coords, this.markerButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.markerClicked();
                } else if (this.hotIndex == 2 &&
                           this.areCoordsInsideRect(coords, this.websiteButton.position, 1.15f, 1.1f, 1.15f, 1.1f)) {
                    this.websiteClicked();
                } else if (this.hotIndex == 100 &&
                           (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                            this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.24f, 5.44f, 2.24f, 5.44f))) {
                    this.Deactivate();
                }
            } else {
                if (this.hotIndex == 3 &&
                    this.areCoordsInsideRect(coords, this.downloadAllButton.position, 1.3f, 1.1f, 1.3f, 1.1f)) {
                    this.downloadAllClicked();
                } else if (this.hotIndex == 4 &&
                           this.areCoordsInsideRect(coords, this.deleteAllButton.position, 1.3f, 1.1f, 1.3f, 1.1f)) {
                    this.deleteAllClicked();
                } else if (this.hotIndex == 90 &&
                           this.areCoordsInsideRect(coords, this.backButton.position, 0.0f, 1.6f, 0.6f, 0.0f)) {
                    this.backClicked();
                } else if (this.hotIndex == 100 &&
                           (this.areCoordsInsideRect(coords, this.closeButton.position, 0.15f, 0.15f, 0.45f, 0.45f) ||
                            this.areCoordsOutsideRect(coords, this.selfTransform.position, 2.24f, 5.44f, 2.24f, 5.44f))) {
                    this.Deactivate();
                }
            }
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
