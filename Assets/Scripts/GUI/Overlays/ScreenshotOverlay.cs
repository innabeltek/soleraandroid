﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
//using Prime31;

public class ScreenshotOverlay : Overlay
{
    public event Action GalleryEvent, FacebookEvent;



    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.cameraRoll = this.baseTransform.Find("Content/Social Box/Camera Roll");
        this.facebook = this.baseTransform.Find("Content/Social Box/Facebook");

        this.overlayMaterial = this.baseTransform.Find("Content/Screenshot Holder/default").GetComponent<MeshRenderer>().material;
    }

    public void Activate(Texture2D tex = null)
    {
        base.Activate();

        this.SetTexture(tex);
    }

    public void SetTexture(Texture2D tex)
    {
        if (tex != null) {
#if UNITY_ANDROID 
            this.ResizeScreenshotThumb(tex.width, tex.height);
#endif
            this.overlayMaterial.mainTexture = tex;

            this.baseTransform.gameObject.SetActive(true);
        }
    }

    public void AlbumFinished()
    {
        this.SetClicksLocked(false);
    }

    public void FacebookFinished()
    {
        this.SetClicksLocked(false);
    }



    void albumClicked()
    {
        this.SetClicksLocked(true);

        if (this.GalleryEvent != null) {
            this.GalleryEvent();
        }
    }

    void facebookClicked()
    {
        this.SetClicksLocked(true);

        if (this.FacebookEvent != null) {
            this.FacebookEvent();
        }
    }

    void ResizeScreenshotThumb(float width, float height)
    {
        Transform left = this.baseTransform.Find("Frame/Left"), right = this.baseTransform.Find("Frame/Right");
        Transform topLeft = this.baseTransform.Find("Frame/Top Left"), topRight = this.baseTransform.Find("Frame/Top Right");
        Transform bottomLeft = this.baseTransform.Find("Frame/Bottom Left"), bottomRight = this.baseTransform.Find("Frame/Bottom Right");

        Transform overlay = this.baseTransform.Find("Content/Screenshot Holder/default");

        tk2dTiledSprite	top = this.baseTransform.Find("Frame/Top").GetComponent<tk2dTiledSprite>();
        tk2dTiledSprite bottom = this.baseTransform.Find("Frame/Bottom").GetComponent<tk2dTiledSprite>();
        tk2dTiledSprite inside = this.baseTransform.Find("Frame/Inside").GetComponent<tk2dTiledSprite>();
		
        float ratio = width / height;
        float newHeight = 7.68f, newWidth = Mathf.Round(newHeight * ratio * 100.0f) / 100.0f;
		
        top.dimensions = new Vector2((newWidth * 100.0f + 64.0f) / this.gui.RetinaConstant, 20.0f / this.gui.RetinaConstant);
        bottom.dimensions = new Vector2((newWidth * 100.0f + 64.0f) / this.gui.RetinaConstant, 20.0f / this.gui.RetinaConstant);
        inside.dimensions = new Vector2((newWidth * 100.0f + 76.0f) / this.gui.RetinaConstant, 832.0f / this.gui.RetinaConstant + 6.0f);
        overlay.localScale = new Vector3(newWidth / 10.24f, 1.0f, 1.0f);
        left.localPosition = new Vector3(newWidth / -2.0f - 0.32f, 0.0f, 0.0f);
        topLeft.localPosition = new Vector3(newWidth / -2.0f - 0.32f, 4.16f, 0.0f);
        bottomLeft.localPosition = new Vector3(newWidth / -2.0f - 0.32f, -4.16f, 0.0f);
        right.localPosition = new Vector3(newWidth / 2.0f + 0.32f, 0.0f, 0.0f);
        topRight.localPosition = new Vector3(newWidth / 2.0f + 0.32f, 4.16f, 0.0f);
        bottomRight.localPosition = new Vector3(newWidth / 2.0f + 0.32f, -4.16f, 0.0f);
    }



    Transform cameraRoll, facebook;
    Material overlayMaterial;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1 && !this.clicksLocked) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;
			
            if (x < this.cameraRoll.position.x + 0.90f && x > this.cameraRoll.position.x - 0.90f &&
                y < this.cameraRoll.position.y + 1.13f && y > this.cameraRoll.position.y - 1.13f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (x < this.facebook.position.x + 0.90f && x > this.facebook.position.x - 0.90f &&
                       y < this.facebook.position.y + 1.13f && y > this.facebook.position.y - 1.13f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            } else if (x > transform.position.x + 5.44f || x < transform.position.x - 5.44f ||
                       y > transform.position.y + 4.16f || y < transform.position.y - 4.16f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.cameraRoll.position, 1.13f, 0.90f, 1.13f, 0.90f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (this.areCoordsInsideRect(coords, this.facebook.position, 1.13f, 0.90f, 1.13f, 0.90f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            } else if (this.areCoordsOutsideRect(coords, this.selfTransform.position, 4.16f, 5.44f, 4.16f, 5.44f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }
		
        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        if (!this.clicksLocked) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

            if (this.hotIndex == 0 &&
                x < this.cameraRoll.position.x + 0.90f && x > this.cameraRoll.position.x - 0.90f &&
                y < this.cameraRoll.position.y + 1.13f && y > this.cameraRoll.position.y - 1.13f) {
                AlbumClicked();
            } else if (this.hotIndex == 1 &&
                       x < this.facebook.position.x + 0.90f && x > this.facebook.position.x - 0.90f &&
                       y < this.facebook.position.y + 1.13f && y > this.facebook.position.y - 1.13f) {
                FacebookClicked();
            } else if (this.hotIndex == 100 &&
                       (x > transform.position.x + 5.44f || x < transform.position.x - 5.44f ||
                       y > transform.position.y + 4.16f || y < transform.position.y - 4.16f)) {
                Deactivate();
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.hotIndex == 0 &&
                this.areCoordsInsideRect(coords, this.cameraRoll.position, 1.13f, 0.90f, 1.13f, 0.90f)) {
                this.albumClicked();
            } else if (this.hotIndex == 1 &&
                       this.areCoordsInsideRect(coords, this.facebook.position, 1.13f, 0.90f, 1.13f, 0.90f)) {
                this.facebookClicked();
            } else if (this.hotIndex == 100 &&
                       this.areCoordsOutsideRect(coords, this.selfTransform.position, 4.16f, 5.44f, 4.16f, 5.44f)) {
                this.Deactivate();
            }
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
