using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class ProgressOverlay : Overlay
{
    public event Action CancelEvent;



    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.cancelButton = this.baseTransform.Find("Content/Cancel Button");
        this.markerAxis = this.baseTransform.Find("Content/Marker Axis");
    }

    public void Activate(int position, int total, bool showCancelButton = true, bool showUnderlay = true)
    {
        base.Activate(showUnderlay);

        this.position = 0;
        this.total = total;

        this.cancelButton.gameObject.SetActive(showCancelButton);

        UpdateProgress(position);

        this.baseTransform.gameObject.SetActive(true);
    }

    public void Cancel()
    {
        if (this.CancelEvent != null) {
            this.CancelEvent();
        }
        this.CancelEvent = null;

        this.Deactivate();
    }

    public void UpdateProgress(int position)
    {
        this.position = position.Clamp(0, this.total);
        float offset = ((float)this.position) / ((float)this.total) * 22.0f;

        this.markerAxis.localRotation = Quaternion.Euler(0.0f, 0.0f, -18.0f - offset);
    }



    int position, total;

    Transform cancelButton, markerAxis;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1 && this.cancelButton.gameObject.activeSelf) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;
			
            if (x > this.cancelButton.position.x - 0.8f && x < this.cancelButton.position.x + 0.8f &&
                y > this.cancelButton.position.y - 0.3f && y < this.cancelButton.position.y + 0.3f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.cancelButton.position, 0.3f, 0.8f, 0.3f, 0.8f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }
		
        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

        if (this.hotIndex == 100 &&
            x > this.cancelButton.position.x - 0.8f && x < this.cancelButton.position.x + 0.8f &&
            y > this.cancelButton.position.y - 0.3f && y < this.cancelButton.position.y + 0.3f) {
            Cancel();
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (this.hotIndex == 100 &&
            this.areCoordsInsideRect(coords, this.cancelButton.position, 0.3f, 0.8f, 0.3f, 0.8f)) {
            this.Cancel();
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
