using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class YesNoOverlay : Overlay
{
    public event Action<bool> ResultEvent;



    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.yesButton = this.baseTransform.Find("Content/Yes Button");
        this.noButton = this.baseTransform.Find("Content/No Button");
    }

    public void Activate(string title, string body, bool showunderlay = true)
    {
        base.Activate(showunderlay);
		
        tk2dTextMesh txtTitle = this.baseTransform.Find("Content/Title").GetComponent<tk2dTextMesh>();
        tk2dTextMesh txtBody = this.baseTransform.Find("Content/Body").GetComponent<tk2dTextMesh>();

        txtTitle.text = title;
        txtBody.text = body;
		
        this.baseTransform.gameObject.SetActive(true);
    }



    void buttonClicked(bool result)
    {
        this.Deactivate();

        if (this.ResultEvent != null) {
            this.ResultEvent(result);
            this.ResultEvent = null;
        }
    }



    Transform yesButton, noButton;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

            if (x > this.yesButton.position.x - 1.0f && x < this.yesButton.position.x + 1.0f &&
                y > this.yesButton.position.y && y < this.yesButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (x > this.noButton.position.x - 1.0f && x < this.noButton.position.x + 1.0f &&
                       y > this.noButton.position.y && y < this.noButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.yesButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 0;
            } else if (this.areCoordsInsideRect(coords, this.noButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 1;
            }
        }

        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

        if (this.hotIndex == 0 &&
            x > this.yesButton.position.x - 1.0f && x < this.yesButton.position.x + 1.0f &&
            y > this.yesButton.position.y && y < this.yesButton.position.y + 0.6f) {
            ButtonClicked(true);
            Deactivate();
        } else if (this.hotIndex == 1 &&
                   x > this.noButton.position.x - 1.0f && x < this.noButton.position.x + 1.0f &&
                   y > this.noButton.position.y && y < this.noButton.position.y + 0.6f) {
            ButtonClicked(false);
            Deactivate();
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (this.hotIndex == 0 &&
            this.areCoordsInsideRect(coords, this.yesButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
            this.buttonClicked(true);
            this.Deactivate();
        } else if (this.hotIndex == 1 &&
                   this.areCoordsInsideRect(coords, this.noButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
            this.buttonClicked(false);
            this.Deactivate();
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
