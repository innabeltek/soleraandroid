﻿using UnityEngine;
using System.Collections;

public class TutorialOverlay : Overlay
{
    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.pages = new Transform[6];
        this.pages[0] = this.baseTransform.Find("Tutorial 01");
        this.pages[1] = this.baseTransform.Find("Tutorial 02");
        this.pages[2] = this.baseTransform.Find("Tutorial 03");
        this.pages[3] = this.baseTransform.Find("Tutorial 04");
        this.pages[4] = this.baseTransform.Find("Tutorial 05");
        this.pages[5] = this.baseTransform.Find("Tutorial 06");
		
        this.nextButton = this.pages[0].Find("Upper Right/Next Button");
        this.skipButton = this.pages[0].Find("Upper Right/Skip Button");
    }

    public void Activate()
    {
        base.Activate();

        this.currentPage = 0;

        this.pages[0].Find("Left").localPosition = new Vector3(this.gui.Width / -200.0f, 0.0f, 0.0f);
        this.pages[0].Find("Right").localPosition = new Vector3(this.gui.Width / 200.0f, 0.0f, 0.0f);
        this.pages[0].Find("Upper Right").localPosition = new Vector3(this.gui.Width / 200.0f, this.gui.Height / 200.0f, 0.0f);
        this.pages[0].gameObject.SetActive(true);

        this.baseTransform.gameObject.SetActive(true);
    }



    int currentPage;

    Transform[] pages;
    Transform nextButton, skipButton;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

            if (x < this.nextButton.position.x && x > this.nextButton.position.x - 3.5f &&
                y < this.nextButton.position.y && y > this.nextButton.position.y - 1.0f) {
                this.touchID = touch.fingerId;
                this.hotIndex = (this.currentPage < 5 ? 0 : 100);
            } else if (this.skipButton != null &&
                       x < this.skipButton.position.x && x > this.skipButton.position.x - 3.5f &&
                       y < this.skipButton.position.y && y > this.skipButton.position.y - 1.0f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.nextButton.position, 0.0f, 0.0f, 1.0f, 3.5f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = (this.currentPage < 5 ? 0 : 100);
            } else if (this.skipButton != null &&
                       this.areCoordsInsideRect(coords, this.skipButton.position, 0.0f, 0.0f, 1.0f, 3.5f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }

        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

        if (this.hotIndex == 0 &&
            x < this.nextButton.position.x && x > this.nextButton.position.x - 3.5f &&
            y < this.nextButton.position.y && y > this.nextButton.position.y - 1.0f) {
            this.pages[this.currentPage].gameObject.SetActive(false);
            this.currentPage++;

            this.pages[this.currentPage].Find("Left").localPosition = new Vector3(this.gui.Width / -200.0f, 0.0f, 0.0f);
            this.pages[this.currentPage].Find("Right").localPosition = new Vector3(this.gui.Width / 200.0f, 0.0f, 0.0f);
            this.pages[this.currentPage].Find("Upper Right").localPosition = new Vector3(this.gui.Width / 200.0f, this.gui.Height / 200.0f, 0.0f);
            this.pages[this.currentPage].gameObject.SetActive(true);

            if (this.currentPage < 5) {
                this.nextButton = this.pages[this.currentPage].Find("Upper Right/Next Button");
                this.skipButton = this.pages[this.currentPage].Find("Upper Right/Skip Button");
            } else {
                this.nextButton = this.pages[this.currentPage].Find("Upper Right/Finish Button");
                this.skipButton = null;
            }
        } else if (this.hotIndex == 100 && this.currentPage >= 5 &&
                   x < this.nextButton.position.x && x > this.nextButton.position.x - 3.5f &&
                   y < this.nextButton.position.y && y > this.nextButton.position.y - 1.0f) {
            Deactivate();
        } else if (this.hotIndex == 100 && this.skipButton != null &&
                   x < this.skipButton.position.x && x > this.skipButton.position.x - 3.5f &&
                   y < this.skipButton.position.y && y > this.skipButton.position.y - 1.0f) {
            Deactivate();
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (this.hotIndex == 0 &&
            this.areCoordsInsideRect(coords, this.nextButton.position, 0.0f, 0.0f, 1.0f, 3.5f)) {
            this.pages[this.currentPage].gameObject.SetActive(false);
            this.currentPage++;

            this.pages[this.currentPage].Find("Left").localPosition = new Vector3(this.gui.Width / -200.0f, 0.0f, 0.0f);
            this.pages[this.currentPage].Find("Right").localPosition = new Vector3(this.gui.Width / 200.0f, 0.0f, 0.0f);
            this.pages[this.currentPage].Find("Upper Right").localPosition = new Vector3(this.gui.Width / 200.0f, this.gui.Height / 200.0f, 0.0f);
            this.pages[this.currentPage].gameObject.SetActive(true);

            if (this.currentPage < 5) {
                this.nextButton = this.pages[this.currentPage].Find("Upper Right/Next Button");
                this.skipButton = this.pages[this.currentPage].Find("Upper Right/Skip Button");
            } else {
                this.nextButton = this.pages[this.currentPage].Find("Upper Right/Finish Button");
                this.skipButton = null;
            }
        } else if (this.hotIndex == 100 && this.currentPage >= 5 &&
                   this.areCoordsInsideRect(coords, this.nextButton.position, 0.0f, 0.0f, 1.0f, 3.5f)) {
            Deactivate();
        } else if (this.hotIndex == 100 && this.skipButton != null &&
                   this.areCoordsInsideRect(coords, this.skipButton.position, 0.0f, 0.0f, 1.0f, 3.5f)) {
            Deactivate();
        }

        this.touchID = this.hotIndex = -1;
    }
#endregion
}
