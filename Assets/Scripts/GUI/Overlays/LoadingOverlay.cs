﻿using UnityEngine;
using System.Collections;

public class LoadingOverlay : Overlay
{
    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);
    }

    public void Activate()
    {
        base.Activate();
		
        this.baseTransform.gameObject.SetActive(true);
    }
}
