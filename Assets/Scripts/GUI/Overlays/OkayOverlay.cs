using UnityEngine;
using System.Collections;
using System.IO;

public class OkayOverlay : Overlay
{
    public new void Initialize(Gui gui, InputManager input)
    {
        base.Initialize(gui, input);

        this.okayButton = this.baseTransform.Find("Content/Okay Button");
    }

    public void Activate(string title, string body, bool showUnderlay = true)
    {
        base.Activate(showUnderlay);

        tk2dTextMesh txtTitle = this.baseTransform.Find("Content/Title").GetComponent<tk2dTextMesh>();
        tk2dTextMesh txtBody = this.baseTransform.Find("Content/Body").GetComponent<tk2dTextMesh>();

        txtTitle.text = title;
        txtBody.text = body;

        this.baseTransform.gameObject.SetActive(true);
    }



    Transform okayButton;



#region INPUT
    public override bool OnTouchStart(Touch touch)
    {
        if (this.touchID == -1) {
            /*
            float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;
			
            if (x > this.okayButton.position.x - 1.0f && x < this.okayButton.position.x + 1.0f &&
                y > this.okayButton.position.y && y < this.okayButton.position.y + 0.6f) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
            */

            Vector2 coords = this.calculateCoordinates(touch.position);

            if (this.areCoordsInsideRect(coords, this.okayButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
                this.touchID = touch.fingerId;
                this.hotIndex = 100;
            }
        }
		
        return true;
    }

    public override void OnTouchMove(Touch touch)
    {
    }

    public override void OnTouchCancel(int fingerid)
    {
        this.touchID = this.hotIndex = -1;
    }

    public override void OnTouchEnd(Touch touch)
    {
        /*
        float x = (touch.position.x * this.gui.RetinaConstant - this.gui.Width / 2.0f) / 100.0f, y = (touch.position.y * this.gui.RetinaConstant - this.gui.Height / 2.0f) / 100.0f;

        if (this.hotIndex == 100 &&
            x > this.okayButton.position.x - 1.0f && x < this.okayButton.position.x + 1.0f &&
            y > this.okayButton.position.y && y < this.okayButton.position.y + 0.6f) {
            Deactivate();
        }
        */

        Vector2 coords = this.calculateCoordinates(touch.position);

        if (this.hotIndex == 100 &&
            this.areCoordsInsideRect(coords, this.okayButton.position, 0.6f, 1.0f, 0.0f, 1.0f)) {
            this.Deactivate();
        }

        this.touchID = this.hotIndex = -1;
    }

#endregion
}
