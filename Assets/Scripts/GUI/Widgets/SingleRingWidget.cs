using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SingleRingWidget : MonoBehaviour
{
    public event Action<string> ItemSelectedEvent;



    public void Initialize(Rotator itemRotator)
    {
        this.itemRotator = itemRotator;

        this.itemRotator.UpdateEvent += new Action<string>(this.ItemUpdate);
    }

    public void Load(string title, ItemList[] itemList, string activeItem)
    {
        this.itemRotator.Preload(title, itemList, activeItem);
    }

    public void Activate()
    {
        this.gameObject.SetActive(true);

        this.itemRotator.Activate();
    }

    public void Activate(string title, ItemList[] itemList, string activeItem)
    {
        this.gameObject.SetActive(true);

        this.itemRotator.Activate(title, itemList, activeItem);
    }

    public void Deactivate()
    {
        this.itemRotator.Deactivate();

        this.gameObject.SetActive(false);
    }

    public void SetVisibility(bool isVisible)
    {
        Renderer[] renderers = this.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < renderers.Length; ++i) {
            renderers[i].enabled = isVisible;
        }
    }

    public Rotator GetCurrentRotator()
    {
        return this.itemRotator;
    }

    public void ItemUpdate(string one)
    {
        if (this.ItemSelectedEvent != null) {
            this.ItemSelectedEvent(one);
        }
    }



    Rotator itemRotator;
}