using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DoubleRingWidget : MonoBehaviour
{
    public event Action<string> CategorySelectedEvent;
    public event Action<string, string> ItemSelectedEvent;

    public string CurrentCategory { get { return this.currentCategory; } }



    public void Initialize(Rotator categoryRotator, Rotator itemRotator)
    {
        this.whichRotator = 0;
        this.currentCategory = "";

        this.rotators = new Rotator[2];

        categoryRotator.UpdateEvent += new Action<string>(CategoryUpdate);
        this.rotators[0] = categoryRotator;

        itemRotator.UpdateEvent += new Action<string>(ItemUpdate);
        this.rotators[1] = itemRotator;
    }

    public void Load(string title, ItemList[] itemList, string activeItem)
    {
        this.rotators[0].Preload(title, itemList, activeItem);
    }

    public void Activate()
    {
        this.gameObject.SetActive(true);

        this.rotators[0].Deactivate();
        this.rotators[1].Deactivate();

        this.rotators[this.whichRotator].Activate();
    }

    public void Activate(int which, string title, ItemList[] itemList, string activeItem)
    {
        this.gameObject.SetActive(true);

        this.rotators[0].Deactivate();
        this.rotators[1].Deactivate();

        this.rotators[which].Activate(title, itemList, activeItem);

        this.whichRotator = which;
    }

    public void Deactivate()
    {
        this.rotators[0].Deactivate();
        this.rotators[1].Deactivate();

        this.gameObject.SetActive(false);
    }

    public void SetVisibility(bool isVisible)
    {
        Renderer[] renderers = this.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < renderers.Length; ++i) {  
            renderers[i].enabled = isVisible;
        }
    }

    public Rotator GetCurrentRotator()
    {
        return this.rotators[this.whichRotator];
    }



    void CategoryUpdate(string category)
    {
        this.currentCategory = category;
        if (this.CategorySelectedEvent != null) {
            this.CategorySelectedEvent(category);
        }
    }

    void ItemUpdate(string item)
    {
        if (item == "BACK") {
            this.rotators[1].Deactivate();
            this.rotators[0].Activate();
            this.whichRotator = 0;
        } else if (this.ItemSelectedEvent != null) {
            this.ItemSelectedEvent(this.currentCategory, item);
        }
    }



    string currentCategory;

    int whichRotator;

    Rotator[] rotators;
}