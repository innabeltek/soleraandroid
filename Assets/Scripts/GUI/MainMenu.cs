﻿using UnityEngine;
using System;
using System.Collections;

public class MainMenu : MonoBehaviour, IInputtable
{
    public event Action<int, bool> WidgetChangedEvent;
    public event Action MenuClickedEvent, CameraClickedEvent, ArrowsClickedEvent;



    public void Initialize(Gui gui)
    {
        this.gui = gui;

        this.touchID = -1;
        this.hotIndex = -1;
        this.selectedButton = -1;

        this.identifiers = new string[3];
        this.identifiers[0] = "Edge";
        this.identifiers[1] = "Sink";
        this.identifiers[2] = "Counter";

        Transform transform = this.transform;

        GameObject menuRing = (GameObject)Instantiate(Resources.Load("Prefabs/GUI/" + this.gui.RetinaString + "/Rings/Menu Ring"), new Vector3((float)Screen.width * this.gui.RetinaConstant / 200.0f, 0.0f, 0.0f), Quaternion.identity);
        menuRing.transform.parent = transform;
        menuRing.name = "Menu Ring";

        this.widgetButtons = new tk2dSprite[3];
        for (int i = 0; i < 3; ++i) {
            this.widgetButtons[i] = transform.Find("Menu Ring/Outer Ring/" + this.identifiers[i] + "s").GetComponent<tk2dSprite>();
        }

        this.helpButton = transform.Find("Menu Ring/Inner Ring/Help").GetComponent<tk2dSprite>();
        this.cameraButton = transform.Find("Menu Ring/Inner Ring/Camera").GetComponent<tk2dSprite>();
        this.arrowsButton = transform.Find("Menu Ring/Inner Ring/Arrows").GetComponent<tk2dSprite>();
    }

    public void DeactivateAll()
    {
        if (this.selectedButton != -1) {
            this.widgetButtons[this.selectedButton].SetSprite("Ring - Outer - " + this.identifiers[this.selectedButton] + "s Unselected");
            if (this.WidgetChangedEvent != null) {
                this.WidgetChangedEvent(this.selectedButton, false);
            }

            this.selectedButton = -1;
        }
    }

    public void SetVisibility(bool isVisible)
    {
        Renderer[] renderers = this.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < renderers.Length; ++i) {
            renderers[i].enabled = isVisible;
        }
    }

    public void MenuFinished()
    {
        this.helpButton.SetSprite("Ring - Inner - Help Unselected");
    }

    public void CameraFinished()
    {
        this.cameraButton.SetSprite("Ring - Inner - Camera Unselected");
    }

    public void ArrowsFinished(bool selected)
    {
        this.arrowsButton.SetSprite("Ring - Inner - Arrows " + (selected ? "S" : "Uns") + "elected");
    }



    void WidgetClick(int index)
    {
        if (this.selectedButton != -1) {
            if (this.WidgetChangedEvent != null) {
                this.WidgetChangedEvent(this.selectedButton, false);
            }
            this.widgetButtons[this.selectedButton].SetSprite("Ring - Outer - " + this.identifiers[this.selectedButton] + "s Unselected");
        }

        this.selectedButton = index;

        if (this.WidgetChangedEvent != null) {
            this.WidgetChangedEvent(this.selectedButton, true);
        }
        this.widgetButtons[this.selectedButton].SetSprite("Ring - Outer - " + this.identifiers[this.selectedButton] + "s Selected");
    }

    IEnumerator MenuClick()
    {
        this.helpButton.SetSprite("Ring - Inner - Help Selected");
        yield return null;

        if (this.MenuClickedEvent != null) {
            this.MenuClickedEvent();
        }
    }

    IEnumerator CameraClick()
    {
        this.cameraButton.SetSprite("Ring - Inner - Camera Selected");
        yield return null;

        if (this.CameraClickedEvent != null) {
            this.CameraClickedEvent();
        }
    }

    void ArrowsClick()
    {
        if (this.ArrowsClickedEvent != null) {
            this.ArrowsClickedEvent();
        }
    }



    Gui gui;

    int touchID, hotIndex, selectedButton;

    string[] identifiers;
    tk2dSprite[] widgetButtons;
    tk2dSprite helpButton, cameraButton, arrowsButton;



#region INPUT
    public bool OnTouchStart(Touch touch)
    {
        float x = 192.0f + ((float)Screen.width - touch.position.x) * this.gui.RetinaConstant;
        float y = (touch.position.y - (float)Screen.height / 2.0f) * this.gui.RetinaConstant;
        float x2y2 = Mathf.Pow(x, 2.0f) + Mathf.Pow(y, 2.0f);
		
        if (x2y2 < 173056.0f) { // Mathf.Pow(416.0f, 2.0f) )
            if (this.touchID == -1) {
                this.touchID = touch.fingerId;
                if (x2y2 > 102400.0f) { // Mathf.Pow(320.0f, 2.0f) )
                    if (y > 108.0f) {
                        this.hotIndex = 0;
                    } else if (y > -108.0f) {
                        this.hotIndex = 1;
                    } else {
                        this.hotIndex = 2;
                    }
                } else {
                    if (y > 70.0f) {
                        this.hotIndex = 3;
                    } else if (y > -70.0f) {
                        this.hotIndex = 4;
                    } else {
                        this.hotIndex = 5;
                    }
                }
            }
			
            return true;
        } else if (this.selectedButton != -1) {
            float x2 = touch.position.x * this.gui.RetinaConstant + 720.0f;

            if (Mathf.Pow(x2, 2.0f) + Mathf.Pow(y, 2.0f) > 1290496.0f) { // Mathf.Pow(1136.0f, 2.0f) )
                DeactivateAll();
            }
        }
		
        return false;
    }

    public void OnTouchMove(Touch touch)
    {
    }

    public void OnTouchCancel(int fingerid)
    {
        this.touchID = -1;
        this.hotIndex = -1;
    }

    public void OnTouchEnd(Touch touch)
    {
        float x = 192.0f + ((float)Screen.width - touch.position.x) * this.gui.RetinaConstant;
        float y = (touch.position.y - (float)Screen.height / 2.0f) * this.gui.RetinaConstant;

        this.touchID = -1;

        if (this.hotIndex > -1) {
            float x2y2 = Mathf.Pow(x, 2.0f) + Mathf.Pow(y, 2.0f);
            if (x2y2 < 173056.0f) {
                if (x2y2 > 102400.0f) {
                    if ((this.hotIndex == 0 && y > 108.0f) ||
                        (this.hotIndex == 1 && y > -108.0f) ||
                        (this.hotIndex == 2 && y <= -108.0f)) {
                        if (this.hotIndex == this.selectedButton) {
                            DeactivateAll();
                        } else {
                            WidgetClick(this.hotIndex);
                        }
                        return;
                    }
                } else {
                    if ((this.hotIndex == 3 && y > 70.0f) ||
                        (this.hotIndex == 4 && y > -70.0f) ||
                        (this.hotIndex == 5 && y <= -70.0f)) {
                        switch (this.hotIndex) {
                            case 3:
                                DeactivateAll();
                                StartCoroutine(MenuClick());
                                break;
                            case 4:
                                StartCoroutine(CameraClick());
                                break;
                            case 5:
                                ArrowsClick();
                                break;
                        }
                        return;
                    }
                }
            }
        }
    }
#endregion
}
