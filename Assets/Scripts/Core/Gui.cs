using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Gui
{
    public bool LeftRingActive { get { return this.leftRingActive; } set { this.leftRingActive = value; } }
    public bool ReticleActive { get { return this.reticleActive; } }

    public string RetinaString { get { return this.retinaString; } }
    public float RetinaConstant { get { return this.retinaConstant; } }
    public float Width { get { return this.width; } }
    public float Height { get { return this.height; } }



    public void Initialize(InputManager input, VuforiaManager vuforia)
    {
        this.input = input;

        this.leftRingActive = false;
        this.reticleActive = true;

        if (Screen.width < 2048) {
            Debug.Log("Monitor " + "Retina");
            this.retinaString = "Retina";
            this.retinaConstant = 2.0f;
        } else {
            this.retinaString = "Retina";
            this.retinaConstant = 1.0f;
        }
        this.width = (float)Screen.width * this.retinaConstant;
        this.height = (float)Screen.height * this.retinaConstant;

        Camera upperCamera = GameObject.Find("GUI Camera Upper").GetComponent<Camera>();
        Camera lowerCamera = GameObject.Find("GUI Camera Lower").GetComponent<Camera>();
		
        upperCamera.orthographicSize = lowerCamera.orthographicSize = this.height / 200.0f;

        this.reticle = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Reticle"), Vector3.zero, Quaternion.identity);
        UpdateReticle();

        this.trackingActive = false;

        this.loading = null;
        this.progress = null;
        this.okay = null;
        this.yesNo = null;
        this.tutorial = null;

        vuforia.TrackEvent += new Action<bool>(UpdateTracking);
    }

    public void UpdateReticle()
    {
        if (this.reticle != null) {
            this.reticle.SetActive(this.reticleActive && !this.trackingActive);
        }
    }

    public void UpdateReticle(bool state)
    {
        this.reticleActive = state;

        this.UpdateReticle();
    }

    public void UpdateTracking(bool state)
    {
        this.trackingActive = state;

        this.UpdateReticle();
    }

    public CountertopInfoOverlay CreateCountertopInfoOverlay()
    {
        CountertopInfoOverlay overlay = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Countertop Info"), Vector3.zero, Quaternion.identity)).GetComponent<CountertopInfoOverlay>();
        overlay.Initialize(this, this.input);

        return overlay;
    }

    public MenuOverlay CreateMenuOverlay()
    {
        MenuOverlay overlay = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Menu Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<MenuOverlay>();
        overlay.Initialize(this, this.input);

        return overlay;
    }

    public ScreenshotOverlay CreateScreenshotOverlay()
    {
        ScreenshotOverlay overlay = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Screenshot Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<ScreenshotOverlay>();
        overlay.Initialize(this, this.input);

        return overlay;
    }

    public SinkInfoOverlay CreateSinkInfoOverlay()
    {
        SinkInfoOverlay overlay = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Sink Info"), Vector3.zero, Quaternion.identity)).GetComponent<SinkInfoOverlay>();
        overlay.Initialize(this, this.input);

        return overlay;
    }

    public Rotator CreateRotator(string type, Transform parent)
    {
        Rotator rotator = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Rings/" + type), new Vector3((float)Screen.width * this.retinaConstant / -200.0f, 0.0f, 0.0f), Quaternion.identity)).GetComponent<Rotator>();
        rotator.Initialize(this, this.input, parent);

        return rotator;
    }

    public void ShowLoadingOverlay()
    {
        if (this.loading == null) {
            this.loading = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Loading Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<LoadingOverlay>();
            this.loading.Initialize(this, this.input);
            this.loading.Activate();
        }
    }

    public void HideLoadingOverlay()
    {
        if (this.loading != null) {
            this.loading.Deactivate();
            this.loading = null;
        }
    }

    public void ShowProgressOverlay(int start, int end, bool showCancel, Action onCancel = null)
    {
        if (this.progress == null) {
            this.progress = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Progress Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<ProgressOverlay>();
            this.progress.Initialize(this, this.input);
            if (showCancel && onCancel != null) {
                this.progress.CancelEvent += onCancel;
            }
            this.progress.Activate(start, end, showCancel);
        }
    }

    public void UpdateProgressOverlay(int delta)
    {
        if (this.progress != null) {
            this.progress.UpdateProgress(delta);
        }
    }

    public void HideProgressOverlay()
    {
        if (this.progress != null) {
            this.progress.Deactivate();
            this.progress = null;
        }
    }

    public void ShowOkayOverlay(string title, string message, Action onEnd = null)
    {
        if (this.okay == null) {
            this.okay = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Okay Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<OkayOverlay>();
            this.okay.Initialize(this, this.input);
            if (onEnd != null) {
                this.okay.EndEvent += onEnd;
            }
            this.okay.EndEvent += () => { this.okay = null; };
            this.okay.Activate(title, message);
        }
    }

    public void ShowYesNoOverlay(string title, string message, Action<bool> onResult = null)
    {
        if (this.yesNo == null) {
            this.yesNo = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/YesNo Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<YesNoOverlay>();
            this.yesNo.Initialize(this, this.input);
            if (onResult != null) {
                this.yesNo.ResultEvent += onResult;
            }
            this.yesNo.EndEvent += () => { this.yesNo = null; };
            this.yesNo.Activate(title, message);
        }
    }

    public void ShowTutorialOverlay(Action onEnd = null)
    {
        if (this.tutorial == null) {
            this.tutorial = ((GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/GUI/" + this.retinaString + "/Overlays/Tutorial Overlay"), Vector3.zero, Quaternion.identity)).GetComponent<TutorialOverlay>();
            this.tutorial.Initialize(this, this.input);
            if (onEnd != null) {
                this.tutorial.EndEvent += onEnd;
            }
            this.tutorial.EndEvent += () => { this.tutorial = null; };
            this.tutorial.Activate();
        }
    }



    bool leftRingActive;
    bool reticleActive;

    string retinaString;
    float retinaConstant;
    float width;
    float height;


    InputManager input;

    GameObject reticle;

    bool trackingActive;

    LoadingOverlay loading;
    ProgressOverlay progress;
    OkayOverlay okay;
    YesNoOverlay yesNo;
    TutorialOverlay tutorial;
}
