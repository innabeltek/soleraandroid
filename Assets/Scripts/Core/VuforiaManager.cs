﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.UI;

public class VuforiaManager : MonoBehaviour, ITrackableEventHandler
{
    
    public bool TrackingActive { get { return this.trackingActive; } }

    public event Action<bool> TrackEvent;
    public GameObject rootObj;



    public void Initialize()
    {
        this.trackingActive = false;
    }

    public IEnumerator Load()
    {
/*#if UNITY_EDITOR_OSX
        Debug.Log("EDITOR SHORTCUT");
        this.trackingActive = true;
        if (this.TrackEvent != null) {
            this.TrackEvent(this.trackingActive);
        }
        yield break;
#endif*/
        ObjectTracker objectTracker;

        do {
            objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            if (objectTracker == null) {
                Debug.Log("Waiting...");
                yield return new WaitForSeconds(1.0f);
            }
        } while(objectTracker == null);

        DataSet dataSet = objectTracker.CreateDataSet();

#if UNITY_IOS 
        if (!dataSet.Load("QCAR/Solera_Vision.xml", Vuforia.VuforiaUnity.StorageType.STORAGE_APPRESOURCE)) {
            Debug.LogError("Failed to load dataset");
        }
#elif UNITY_ANDROID 
        if (!dataSet.Load("QCAR/Solera_Vision.xml", Vuforia.VuforiaUnity.StorageType.STORAGE_APPRESOURCE))
        {
            Debug.LogError("Failed to load dataset");
        }
        /*Debug.Log("Loading Dataset...");
		if (!dataSet.Load(Application.persistentDataPath + "/QCAR/Solera_Vision.xml", Vuforia.VuforiaUnity.StorageType.STORAGE_ABSOLUTE)) {
			Debug.LogError ("Failed to load dataset");
		}*/
#endif

        objectTracker.ActivateDataSet(dataSet);

        Transform trunk = GameObject.Find("Trunk").transform;
        Transform root = trunk.Find("Root");
        
        
        // StateManager stateManager = TrackerManager.Instance.GetStateManager();
        IEnumerable<TrackableBehaviour> trackableBehaviors = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();

        foreach (TrackableBehaviour trackableBehavior in trackableBehaviors) {
            /*
            ImageTargetAbstractBehaviour itab = trackableBehavior as ImageTargetAbstractBehaviour;
            Debug.Log("Adding augmentation to " + trackableBehavior.TrackableName);
            Debug.Log("Augmentation dimensions: ");
            Debug.Log(itab.GetSize().ToString());
            */

            Transform trackableTransform = trackableBehavior.transform;

            trackableTransform.localPosition = Vector3.zero;
            trackableTransform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            trackableTransform.localScale = Vector3.one;

            trunk.parent = trackableTransform;
            trunk.localPosition = Vector3.zero;
            trunk.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            trunk.localScale = Vector3.one;

            root.localPosition = Vector3.zero;
            root.localRotation = Quaternion.identity;
            root.localScale = Vector3.one;
            

            trunk.gameObject.SetActive(true);
            //rootObj.GetComponentInChildren<Renderer>().materials[0].shader = Shader.Find("Standard");
            this.trackableBehavior = trackableBehavior;
            this.trackableBehavior.RegisterTrackableEventHandler(this);

            this.trackableBehavior.gameObject.AddComponent<DefaultTrackableEventHandler>();

            break;
        }

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        bool oldTracking = this.trackingActive;
        this.trackingActive = (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED);
        if (this.trackingActive != oldTracking && this.TrackEvent != null) {
            this.TrackEvent(this.trackingActive);
        }
    }



    bool trackingActive;


    TrackableBehaviour trackableBehavior;
}
