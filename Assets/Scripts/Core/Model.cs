using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class Model : MonoBehaviour
{
    public static string /*screenshotPath, currentSink,*/ CountertopBundlePath;
    public string BundlePath { get { return this.bundlePath; } }
    public string CachePath { get { return this.cachePath; } }
    public string SinkBundlePath { get { return this.sinkBundlePath; } }
    public string ScreenshotPath { get { return this.screenshotPath; } }
    public string QCARPath { get { return this.qcarPath; } }

    public string CurrentSinkCategory { get { return this.currentSinkCategory; } }
    public string CurrentSink { get { return this.currentSink; } }
    public string CurrentCountertopCategory { get { return this.currentCountertopCategory; } }
    public string CurrentCountertop { get { return this.currentCountertop; } }
    public string CurrentEdge { get { return this.currentEdge; } }

    public Dictionary<string, ItemList[]> Sinks { get { return this.sinks; } }
    public Dictionary<string, ItemList[]> Countertops { get { return this.countertops; } }
    public Dictionary<string, ItemList[]> Edges { get { return this.edges; } }


    public event Action SinkPresenceUpdated;

   static List<string> loadedSinks = new List<string>();
    

    public void Initialize(Frustum frustum, Gui gui)
    {
        this.frustum = frustum;
        this.gui = gui;

#if UNITY_IPHONE
        this.bundlePath = Application.temporaryCachePath.Replace("/Caches", "") + "/Bundles";
        this.screenshotPath = Application.temporaryCachePath.Replace("/Caches", "") + "/Screenshots";
        this.qcarPath = Application.streamingAssetsPath + "/QCAR";
#elif UNITY_ANDROID && !UNITY_EDITOR
        

        this.bundlePath = Application.persistentDataPath + "/Bundles";
        this.screenshotPath = Application.persistentDataPath + "/Screenshots";
        this.qcarPath = Application.persistentDataPath + "/QCAR";

        this.cachePath = Application.persistentDataPath + "/UnityCache/Shared";
        /* this.bundlePath = Application.streamingAssetsPath + "/Bundles";
         this.screenshotPath = Application.streamingAssetsPath + "/Screenshots";
         this.qcarPath = Application.streamingAssetsPath + "/QCAR";*/
#elif UNITY_EDITOR
        this.bundlePath = Application.streamingAssetsPath + "/Bundles";
        screenshotPath = Application.streamingAssetsPath + "/Screenshots";
        this.qcarPath = Application.streamingAssetsPath + "/QCAR";
#endif
        this.sinkBundlePath = this.bundlePath + "/Sinks";
        CountertopBundlePath = this.bundlePath + "/Countertops";

        Debug.Log("dataPath: " + Application.dataPath);
        Debug.Log("persistentDataPath: " + Application.persistentDataPath);
        Debug.Log("streamingAssetsPath: " + Application.streamingAssetsPath);
        Debug.Log("temporaryCachePath: " + Application.temporaryCachePath);
        Debug.Log("BundlePath: " + this.bundlePath);
        Debug.Log("ScreenshotPath: " + screenshotPath);
        Debug.Log("QCARPath: " + QCARPath);
        Debug.Log("SinkBundlePath: " + this.sinkBundlePath);
        Debug.Log("CountertopBundlePath: " + CountertopBundlePath);

        this.currentSinkCategory = "Bronze";
        this.currentSink = "S759";
        this.currentCountertopCategory = "Beige";
        this.currentCountertop = "Almond Mauve";
        this.currentEdge = "S";

        this.countertops = LoadList((TextAsset)Resources.Load("TextFiles/CountertopList"));
        this.edges = LoadList((TextAsset)Resources.Load("TextFiles/EdgeList"));

        this.countertopInfo = LoadCountertopInfo("TextFiles/CountertopInfo");


        this.manifestBundle = new Bundle(this.bundlePath);

        this.sinkBundle = new Bundle(this.bundlePath);

        this.sinkBundle.LoadStartEvent += this.loadStart;
        this.sinkBundle.LoadEndEvent += this.loadEnd;
        this.sinkBundle.DownloadStartEvent += this.downloadStart;
        this.sinkBundle.DownloadProgressEvent += this.downloadProgress;
        this.sinkBundle.DownloadEndEvent += this.downloadEnd;
    }

    public IEnumerator Load()
    {
#if UNITY_IPHONE
        if (!Directory.Exists(this.screenshotPath)) {
            Directory.CreateDirectory(this.screenshotPath);
            UnityEngine.iOS.Device.SetNoBackupFlag(this.screenshotPath);
        }

        if (!Directory.Exists(this.bundlePath)) {
            Directory.CreateDirectory(this.bundlePath);
            UnityEngine.iOS.Device.SetNoBackupFlag(this.bundlePath);

            File.Copy(Application.streamingAssetsPath + "/Sinks/iOS/sinklist", this.bundlePath + "/sinklist");
            UnityEngine.iOS.Device.SetNoBackupFlag(this.bundlePath + "/sinklist");
        }

        if (!Directory.Exists(this.sinkBundlePath)) {
            Directory.CreateDirectory(this.sinkBundlePath);
            UnityEngine.iOS.Device.SetNoBackupFlag(this.sinkBundlePath);

            string[] sinkModels = Directory.GetFiles(Application.streamingAssetsPath + "/Sinks/iOS/", "*");
            for (int i = 0; i < sinkModels.Length; ++i) {
                string sinkModel = sinkModels[i];
                string destination = this.sinkBundlePath + sinkModel.Substring(sinkModel.LastIndexOf("/"));

                if (destination.Contains("sinklist") || destination.Contains(".")) {
                    continue;
                }

				Debug.Log("Source: " + sinkModel + ", Destination: " + destination);
                File.Copy(sinkModel, destination);
                UnityEngine.iOS.Device.SetNoBackupFlag(destination);
            }
        }

#elif UNITY_ANDROID
		if (!Directory.Exists(screenshotPath)) {
			Directory.CreateDirectory(screenshotPath);
		}

		if (!Directory.Exists(this.bundlePath)) {
			Directory.CreateDirectory(this.bundlePath);
#endif
#if UNITY_ANDROID && UNITY_EDITOR
            Debug.Log("sinklist: " + "file://" + Application.streamingAssetsPath + "/Sinks/Android/sinklist");
			WWW www = new WWW("file://" + Application.streamingAssetsPath + "/Sinks/Android/sinklist");
#elif UNITY_ANDROID && !UNITY_EDITOR
			WWW www = new WWW(Application.streamingAssetsPath + "/Sinks/Android/sinklist");
#endif
#if UNITY_ANDROID 
            Debug.Log("Yielding...");
			yield return www;

            Debug.Log("Testing...");
			if (string.IsNullOrEmpty(www.error)) {
				Debug.Log("www.error isnullorempty!");
				FileStream cache = new FileStream(BundlePath + "/sinklist", FileMode.Create);
				cache.Write(www.bytes, 0, www.bytes.Length);
				cache.Close();
				cache.Dispose();
				www.Dispose();
			}
		}

		if (!Directory.Exists(SinkBundlePath)) {
			Directory.CreateDirectory(SinkBundlePath);


            string[] sinkModels = { "S205", "S106A", "S848BE", "S158", "S098", "S909", "S059", "S0022VW"};
			int i = 0;

			this.gui.ShowProgressOverlay(0, sinkModels.Length, false);

			foreach (string sinkModel in sinkModels) {
                string sinkModelLC = sinkModel.ToLower();
                string destination = SinkBundlePath + "/" + sinkModelLC;
#endif
#if UNITY_ANDROID && UNITY_EDITOR
                WWW www = new WWW("file://" + (string)Application.streamingAssetsPath + "/Sinks/Android/" + sinkModelLC);
#elif UNITY_ANDROID && !UNITY_EDITOR
				WWW www = new WWW((string)Application.streamingAssetsPath + "/Sinks/Android/" + sinkModelLC);
#endif
#if UNITY_ANDROID 
                yield return www;

                Debug.Log(sinkModelLC);
				if (string.IsNullOrEmpty(www.error)) {
                    Debug.Log(sinkModelLC);
					FileStream cache = new FileStream(destination, FileMode.Create);
					cache.Write(www.bytes, 0, www.bytes.Length);
					cache.Close();
					cache.Dispose();
					www.Dispose();
				} else {
					Debug.Log(www.error);
					www.Dispose();
				}
				yield return null;

				++i;
				this.gui.UpdateProgressOverlay(i);
			}
           
			this.gui.HideProgressOverlay();
		}
      
		if (!Directory.Exists(CountertopBundlePath)) {
			Directory.CreateDirectory(CountertopBundlePath);
		}
#endif
        this.gui.ShowLoadingOverlay();
       yield return StartCoroutine(LoadManifest());
        this.gui.HideLoadingOverlay();
    }

    IEnumerator LoadManifest()
    {
        yield return StartCoroutine(this.manifestBundle.Download("sinklist"));
        if (this.manifestBundle.Downloaded) {
            Debug.Log("LoadManifest: Downloaded!");
        } else {
            Debug.Log("LoadManifest: Download Failed!");
        }

        yield return StartCoroutine(this.manifestBundle.Load("sinklist"));
       if (this.manifestBundle.Loaded) {
            Debug.Log("LoadManifest: Loaded!");
            this.sinks = this.LoadList((TextAsset)this.manifestBundle.Asset.LoadAsset("SinkList.txt"));
             this.manifestBundle.Unload();
            this.UpdateSinkPresence();
       } else {
            Debug.Log("LoadManifest: Load Failed!");
       }
        Resources.UnloadUnusedAssets();
    }

    Dictionary<string, ItemList[]> LoadList(TextAsset textdata)
    {
        Dictionary<string, ItemList[]> list = new Dictionary<string, ItemList[]>();
        StringReader reader = null;
        char[] delim = new char[1] { '\t' };
        string txt = "", category = "";
        string[] txt_d;
        int i = -1, j = -1, k = 0, l = 0, sink_count = 0;

        reader = new StringReader(textdata.text);
        if (reader != null) {
            while ((txt = reader.ReadLine()) != null) {
                txt_d = txt.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                if (txt.StartsWith("\t\t\t")) {
                    if (j == -1) {
                        j = l = k = 0;
                        list[category][j] = new ItemList("Solo", sink_count, 0);
                    }
                    list[category][j].Items[k].Name = txt_d[0];
                    if (txt_d.Length > 1 && txt_d[1] == "1") {
                        list[category][j].Items[k].HasCountertop = true;
                    } else {
                        list[category][j].Items[k].HasCountertop = false;
                    }

                    k++;
                } else if (txt.StartsWith("\t\t")) {
                    list[category][j].Breakpoints[l].Index = k;
                    list[category][j].Breakpoints[l].Title = txt_d[0];
                    l++;
                } else if (txt.StartsWith("\t")) {
                    j++;
                    k = l = 0;
                    list[category][j] = new ItemList(txt_d[0], sink_count, System.Convert.ToInt32(txt_d[1]));
                } else {
                    i++;
                    j = -1;
                    k = l = 0;
                    sink_count = System.Convert.ToInt32(txt_d[1]);
                    category = txt_d[0];
                    list.Add(category, new ItemList[System.Convert.ToInt32(txt_d[2])]);
                }
            }
        }
        list.Add("Categories", this.PullHeaders("Categories", list));

        return list;
    }

    ItemList[] PullHeaders(string title, Dictionary<string, ItemList[]> source)
    {
        int i = 0;
        ItemList[] dest = new ItemList[1];

        dest[0] = new ItemList(title, source.Count, 0);
        foreach (KeyValuePair<string, ItemList[]> pair in source) {
            dest[0].Items[i].Name = pair.Key;
            ++i;
        }

        return dest;
    }

    public void UpdateSinkPresence()
    {
        string[] dirFiles = Directory.GetFiles(this.sinkBundlePath);
        List<string> files = new List<string>();

        for (int i = 0; i < dirFiles.Length; ++i) {
            string filename = dirFiles[i].Substring(dirFiles[i].LastIndexOf("/") + 1);
            if (!filename.Contains(".")) {
                files.Add(filename);
            }
        }

#if UNITY_ANDROID && !UNITY_EDITOR
        string[] dirs = Directory.GetDirectories(this.cachePath);
        for (int i = 0; i < dirs.Length; ++i)
        {
            string filename = dirs[i].Substring(dirs[i].LastIndexOf("/") + 1);
            if (!filename.Contains("."))
            {
                files.Add(filename);
            }
        }
#endif
        this.sinkPresence = files.ToArray();

        foreach (KeyValuePair<string, ItemList[]> sinklists in this.sinks) {
            //foreach (ItemList sinklist in sinklists.Value) {
            for (int i = 0; i < sinklists.Value.Length; ++i) {
                for (int j = 0; j < sinklists.Value[i].Items.Length; ++j) {
                    string itemName = sinklists.Value[i].Items[j].Name.ToLower();

                    sinklists.Value[i].Items[j].IsPresent = false;

                    for (int k = 0; k < this.sinkPresence.Length; ++k) {
                        if (itemName == this.sinkPresence[k]) {
                            sinklists.Value[i].Items[j].IsPresent = true;
                            break;
                        }
                    }
                }
            }
        }

        if (SinkPresenceUpdated != null) {
            SinkPresenceUpdated();
        }
    }

    void LoadSinkInfo(TextAsset textdata, Texture2D infoimage)
    {
        if (this.sinkInfo == null) {
            this.sinkInfo = new SinkInfo();
        }

        if (textdata == null && infoimage == null) {
            this.sinkInfo.Category = this.currentSinkCategory;
            this.sinkInfo.Model = this.currentSink;
            this.sinkInfo.Title = this.currentSink;
            this.sinkInfo.Description = "Please download the sink model for more information.";
            this.sinkInfo.Url = "";
            this.sinkInfo.HasCustomCountertops = false;
            this.sinkInfo.InfoTexture = (Texture2D)Resources.Load("Textures/" + this.gui.RetinaString + "/Countertops Info/Question Mark", typeof(Texture2D));

            return;
        }

        string txt;
        int i = 0;
        StringReader reader = new StringReader(textdata.text);

        if (reader != null) {
            while ((txt = reader.ReadLine()) != null) {
                switch (i) {
                    case 0:
                        this.sinkInfo.Model = txt;
                        break;
                    case 1:
                        this.sinkInfo.Category = txt;
                        break;
                    case 2:
                        this.sinkInfo.Title = txt;
                        break;
                    case 3:
                        this.sinkInfo.Description = txt;
                        break;
                    case 4:
                        this.sinkInfo.Url = txt;
                        break;
                    case 5:
                        this.sinkInfo.HasCustomCountertops = (System.Convert.ToInt32(txt) == 1);
                        break;
                }
                i = (i + 1).Wrap(6);
            }
        }

        this.sinkInfo.InfoTexture = infoimage;
    }

    Dictionary<string, CountertopInfo> LoadCountertopInfo(string assetname)
    {
        Dictionary<string, CountertopInfo> list = new Dictionary<string, CountertopInfo>();
        string txt, category = "";
        int i = 0;
        TextAsset textdata = (TextAsset)Resources.Load(assetname, typeof(TextAsset));
        StringReader reader = new StringReader(textdata.text);

        if (reader != null) {
            while ((txt = reader.ReadLine()) != null) {
                switch (i) {
                    case 0:
                        list.Add(txt, new CountertopInfo());
                        category = txt;
                        list[category].Category = txt;
                        break;
                    case 1:
                        list[category].Description = txt;
                        break;
                }
                i = (i + 1).Wrap(2);
            }
        }

        return list;
    }

    public SinkInfo GetSinkInfo()
    {
        return this.sinkInfo;
    }

    public CountertopInfo GetCountertopInfo()
    {
        return this.countertopInfo[this.currentCountertopCategory];
    }

    public bool IsSinkPresent(string sku)
    {
        sku = sku.ToLower();
        for (int i = 0; i < this.sinkPresence.Length; ++i) {
            if (this.sinkPresence[i] == sku) {
                return true;
            }
        }

        return false;
    }

    public void ChangeSink(string category = "", string sink = "")
    {
        if (category == "") {
            category = this.currentSinkCategory;
        } else {
            this.currentSinkCategory = category;
        }

        if (sink == "") {
            sink = this.currentSink;
        } else {
            this.currentSink = sink;
        }

        StartCoroutine(LoadSink(this.currentSinkCategory, this.currentSink));
    }

    public void ChangeCountertop(string category = "", string countertop = "")
    {
        if (category == "") {
            category = this.currentCountertopCategory;
        } else {
            this.currentCountertopCategory = category;
        }

        if (countertop == "") {
            countertop = this.currentCountertop;
        } else {
            this.currentCountertop = countertop;
        }

        LoadCountertop(this.currentCountertopCategory, this.currentCountertop);
    }

    public void ChangeEdge(string edge = "")
    {
        if (edge == "") {
            edge = this.currentEdge;
        } else {
            this.currentEdge = edge;
        }

        LoadEdge(this.currentEdge);
    }

    public IEnumerator LoadSink(string category = "", string sink = "")
    {
        
        if (category == "") {
            category = this.currentSinkCategory;
        }
        if (sink == "") {
            sink = this.currentSink;
        }

        this.frustum.SetSinkActive(false);

        this.gui.ShowLoadingOverlay();
        yield return StartCoroutine(sinkBundle.Load("Sinks/" + sink.ToLower()));
        this.gui.HideLoadingOverlay();

       

        if (sinkBundle.Asset == null) {
            LoadSinkInfo(null, null);
            this.frustum.DisplaySink(null, this.currentEdge);
            Debug.Log("LoadSink: Error loading bundle Sinks/" + sink.ToLower());
            this.gui.ShowOkayOverlay("Load Error!", "There was a problem loading the sink.\nPlease check your internet connection.");
            yield break;
        }

        loadedSinks.Add(sinkBundle.Asset.name);
        LoadSinkInfo((TextAsset)sinkBundle.Asset.LoadAsset("Info"), (Texture2D)sinkBundle.Asset.LoadAsset("Icon " + this.gui.RetinaString));
        this.frustum.DisplaySink(sinkBundle.Asset, this.currentEdge);

        UpdateSinkPresence();
    }

    public void LoadCountertop(string category = "", string countertop = "")
    {
        if (category == "") {
            category = this.currentCountertopCategory;
        }
        if (countertop == "") {
            countertop = this.currentCountertop;
        }

        this.frustum.DisplayCountertop(category, countertop);
    }

    public void LoadEdge(string edge)
    {
        this.frustum.DisplayCountertopEdge(edge, this.sinkBundle.Asset);
    }

    public void DeleteSink(string model)
    {
        string modelLC = model.ToLower();

        if (File.Exists(this.sinkBundlePath + "/" + modelLC)) {
            File.Delete(this.sinkBundlePath + "/" + modelLC);
        }

        if (File.Exists(CountertopBundlePath + "/" + modelLC))
            File.Delete(CountertopBundlePath + "/" + modelLC);

        if (model == this.currentSink) {
            LoadSinkInfo(null, null);
        }

        UpdateSinkPresence();
    }

    public IEnumerator DownloadAllSinks()
    {
        this.gui.ShowLoadingOverlay();
        yield return StartCoroutine(LoadManifest());
        this.gui.HideLoadingOverlay();

        bool stopDownloading = false;

        int sinkCount = 0;
        foreach (KeyValuePair<string, ItemList[]> list in this.sinks) {
            if (list.Key == "Categories") {
                continue;
            }

            sinkCount += list.Value[0].Length;
        }

        this.gui.ShowProgressOverlay(0, sinkCount, true, () => {
            stopDownloading = true;
        });

        Bundle sinkBundle = new Bundle(this.bundlePath);

        sinkCount = 0;
        foreach (KeyValuePair<string, ItemList[]> list in this.sinks) {
            if (stopDownloading) {
                break;
            }
            if (list.Key == "Categories") {
                continue;
            }

            ItemList items = list.Value[0];
            //foreach (Item item in items.Items) {
            for (int i = 0; i < items.Items.Length; ++i) {
                //string itemNameLC = item.Name.ToLower();
                string itemNameLC = items.Items[i].Name.ToLower();
                if (!File.Exists(this.sinkBundlePath + "/" + itemNameLC)) {
                    yield return StartCoroutine(sinkBundle.Load("Sinks/" + itemNameLC));
                    loadedSinks.Add(sinkBundle.Asset.name);
                }

                if (stopDownloading) {
                    break;
                }
                sinkCount++;
                this.gui.UpdateProgressOverlay(sinkCount);
            }
        }

        if (!stopDownloading) {
            this.gui.HideProgressOverlay();
        }

        UpdateSinkPresence();
        Resources.UnloadUnusedAssets();
    }

    public void DeleteAllSinks()
    {
        string[] sinks = Directory.GetFiles(this.sinkBundlePath);
        loadedSinks.Clear();
        foreach (string sink in sinks) {
            File.Delete(sink);
        }

        UpdateSinkPresence();
    }



    void loadStart()
    {
        this.gui.HideLoadingOverlay();
        this.gui.HideProgressOverlay();

        this.gui.ShowLoadingOverlay();
    }

    void loadEnd()
    {
        this.gui.HideLoadingOverlay();
    }

    void downloadStart()
    {
        this.gui.HideLoadingOverlay();
        this.gui.HideProgressOverlay();

        this.gui.ShowProgressOverlay(0, 100, false);
    }

    void downloadProgress(int progress)
    {
        this.gui.UpdateProgressOverlay(progress);
    }

    void downloadEnd()
    {
        this.gui.HideProgressOverlay();
    }



    string bundlePath;
    string cachePath;
    string sinkBundlePath;
    string screenshotPath;
    string qcarPath;

    string currentSinkCategory;
    string currentSink;
    string currentCountertopCategory;
    string currentCountertop;
    string currentEdge;
    string countertopBundlePath;

    Dictionary<string, ItemList[]> sinks;
    Dictionary<string, ItemList[]> countertops;
    Dictionary<string, ItemList[]> edges;


    Frustum frustum;
    Gui gui;

    SinkInfo sinkInfo;
    Dictionary<string, CountertopInfo> countertopInfo;
    string[] sinkPresence;

    Bundle manifestBundle, sinkBundle;
}
