﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

public class Preloader : MonoBehaviour
{

    private string nextScene = "MainScene";

    private bool obbisok = false;
    private bool loading = false;
    private bool replacefiles = false; //true if you wish to over copy each time

    private string[] paths ={
        "Vuforia/Solera_Vision_target.dat",
        "Vuforia/Solera_Vision_target.xml"};





    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Application.dataPath.Contains(".obb") && !obbisok)
            {
                StartCoroutine(CheckSetUp());
                obbisok = true;
            }
        }
        else
        {
            if (!loading)
            {
                StartApp();
            }
        }
    }


    public void StartApp()
    {
        loading = true;
        SceneManager.LoadScene(nextScene);
    }

    public IEnumerator CheckSetUp()
    {
        //Check and install!
        for (int i = 0; i < paths.Length; ++i)
        {
            yield return StartCoroutine(PullStreamingAssetFromObb(paths[i]));
        }
        yield return new WaitForSeconds(3f);
        StartApp();
    }

    //Alternatively with movie files these could be extracted on demand and destroyed or written over
    //saving device storage space, but creating a small wait time.
    public IEnumerator PullStreamingAssetFromObb(string sapath)
    {
        if (!File.Exists(Application.persistentDataPath + sapath) || replacefiles)
        {
            WWW unpackerWWW = new WWW(Application.streamingAssetsPath + "/" + sapath);
            while (!unpackerWWW.isDone)
            {
                yield return null;
            }
            if (!string.IsNullOrEmpty(unpackerWWW.error))
            {
                Debug.Log("Error unpacking:" + unpackerWWW.error + " path: " + unpackerWWW.url);

                yield break; //skip it
            }
            else
            {
                Debug.Log("Extracting " + sapath + " to Persistant Data");

                if (!Directory.Exists(Path.GetDirectoryName(Application.persistentDataPath + "/" + sapath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(Application.persistentDataPath + "/" + sapath));
                }
                File.WriteAllBytes(Application.persistentDataPath + "/" + sapath, unpackerWWW.bytes);
                //could add to some kind of uninstall list?
            }
        }
        yield return 0;
    }
}

/*void Start()
{
#if UNITY_ANDROID && !UNITY_EDITOR
    Debug.Log("Starting...");
    expPath = GooglePlayDownloader.GetExpansionFilePath();
    Debug.Log("expPath: " + expPath);
    if (expPath != null) {
        if (GooglePlayDownloader.GetMainOBBPath(expPath) == null) {
            GooglePlayDownloader.FetchOBB();
        }
    }

#endif
    StartCoroutine(this.LoadLevel());
}

protected IEnumerator LoadLevel()
{
#if UNITY_ANDROID && !UNITY_EDITOR
    string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
    while (mainPath == null) {
        Debug.Log("waiting mainPath " + mainPath);
        yield return new WaitForSeconds(1.0f);
        mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
    }

    Debug.Log("mainPath found: " + mainPath);

    yield return StartCoroutine(InitializeQCAR());
#endif
    SceneManager.LoadScene(1);

    yield break;
}

protected IEnumerator InitializeQCAR()
{
    // TODO: This probably isn't necessary anymore. WWW isn't for sure!
#if UNITY_ANDROID
    Debug.Log("InitializeQCAR Start");

    string QCARPath = Application.persistentDataPath + "/QCAR";

    Debug.Log("QCARPath: " + QCARPath);

    if (!Directory.Exists(QCARPath)) {
        Debug.Log("QCARPath Not Found!");

        Directory.CreateDirectory(QCARPath);

        string[] QCARFiles = { "Solera_Vision.dat", "Solera_Vision.xml", "unity.txt" };
        WWW www;

        foreach (string QCARFile in QCARFiles) {
            string destination = QCARPath + "/" + QCARFile;

            Debug.Log("Source: " + Application.streamingAssetsPath + "/QCAR/" + QCARFile + " Destination: " + destination);

            www = new WWW(Application.streamingAssetsPath + "/QCAR/" + QCARFile);
            yield return www;

            if (string.IsNullOrEmpty(www.error)) {
                Debug.Log("File Exists! Copying...");
                FileStream cache = new FileStream(destination, FileMode.Create);
                cache.Write(www.bytes, 0, www.bytes.Length);
                cache.Close();
                cache.Dispose();
                www.Dispose();
            }
        }
    }

    Debug.Log("InitializeQCAR End");
#endif

    yield break;
}



string expPath;
bool downloadStarted;
}*/
