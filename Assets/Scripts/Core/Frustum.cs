﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class Frustum : MonoBehaviour, IInputtable
{
    public void Initialize(VuforiaManager vuforia)
    {
        this.arrowsActive = false;

        this.root = GameObject.Find("Trunk").transform.Find("Root");
        this.centerArrow = this.root.Find("CenterArrow");
        this.leftArrow = this.root.Find("LeftArrow");
        this.rightArrow = this.root.Find("RightArrow");
        this.leftArrowDown = this.root.Find("LeftArrowDown");
        this.rightArrowDown = this.root.Find("RightArrowDown");

        this.root.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        //this.centerArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        //this.leftArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        //this.rightArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        //this.leftArrowDown.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        //this.rightArrowDown.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);


        this.rootID = this.leftID = this.rightID = this.leftDownID = this.rightDownID = -1;
        this.leftOffset = this.rightOffset = this.leftDownOffset = this.rightDownOffset = 0.0f;
        this.lastRootOffset = this.lastLeftOffset = this.lastRightOffset = this.lastLeftDownOffset = this.lastRightDownOffset = 0.0f;
        this.isLeftDown = this.isRightDown = false;
        this.isLeftTogglable = this.isRightTogglable = false;
        this.trackingActive = false;
        this.sinkActive = true;

        this.leftArrowDown.gameObject.SetActive(false);
        this.rightArrowDown.gameObject.SetActive(false);

        vuforia.TrackEvent += new Action<bool>(this.UpdateTracking);
        this.root.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
    }

    public IEnumerator Load()
    {
        this.root.gameObject.SetActive(true);
        this.root.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        yield break;
    }

    public bool isSinkActive()
    {
        return this.sinkActive;
    }

    public void SetSinkActive(bool state)
    {
        this.sinkActive = state;

        this.UpdateSinkVisibility();
    }

    public void UpdateSinkVisibility()
    {
        this.root.gameObject.SetActive(this.sinkActive);
        this.root.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
    }

    public bool AreArrowsActive()
    {
        return this.arrowsActive;
    }

    public void SetArrows(bool state)
    {
        this.arrowsActive = state;

        this.centerArrow.gameObject.SetActive(state);
        this.leftArrow.gameObject.SetActive(state && !this.isLeftDown);
        this.rightArrow.gameObject.SetActive(state && !this.isRightDown);
        this.leftArrowDown.gameObject.SetActive(state && this.isLeftDown);
        this.rightArrowDown.gameObject.SetActive(state && this.isRightDown);

        /*this.centerArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        this.leftArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        this.rightArrow.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        this.leftArrowDown.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);
        this.rightArrowDown.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,1f);*/

    }



    public void DisplaySink(AssetBundle asset, string edge)
    {
        
        DisplayCountertopEdge(edge, asset);

        float offset = 7.5f;
		
        Destroy(this.currentSink);
        Destroy(this.currentSinkTop);
        Destroy(this.currentDepthMask);
        Destroy(this.currentAccs);

        this.currentSink = this.currentSinkTop = this.currentDepthMask = this.currentAccs = null;

        if (asset == null) {
            Debug.Log("Monitor " + "Asset is null");
            Resources.UnloadUnusedAssets();
            return;
        }

        this.currentSink = (GameObject)Instantiate(asset.LoadAsset("Sink.prefab"), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
        this.currentSinkTop = (GameObject)Instantiate(asset.LoadAsset("Countertop.prefab"), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
        this.currentDepthMask = (GameObject)Instantiate(asset.LoadAsset("Cabinet.prefab"), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
        this.currentAccs = (GameObject)Instantiate(asset.LoadAsset("Accessory.prefab"), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);

        MeshRenderer mesh = this.currentSink.GetComponent<MeshRenderer>();
        if (mesh != null)
        {
            Destroy(mesh);
        }

        if (this.currentSink.GetComponent<Transform>().childCount > 0)
        {
            MeshRenderer[] rendSink;
            rendSink = this.currentSink.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer r in rendSink)
            {
                r.material.shader = Shader.Find("Standard");
                r.material.SetColor("_Color", Color.white);
            }
        }

        if (this.currentSink.GetComponent<Transform>().childCount > 0)
        {
            MeshRenderer[] rendSink;
            rendSink = this.currentSink.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer r in rendSink)
            {
                r.material.shader = Shader.Find("Standard");
            }
        }
        
        if (this.currentDepthMask.GetComponent<Transform>().childCount > 0)
        {
            MeshRenderer[] rendSinkTop;
            rendSinkTop = this.currentDepthMask.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer r in rendSinkTop)
            {
                r.material.shader = Shader.Find("DepthMask");
            }
        }

         if (this.currentSinkTop.GetComponent<Transform>().childCount > 0)
        {
            MeshRenderer[] rendSinkTop;
            rendSinkTop = this.currentSinkTop.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer r in rendSinkTop)
            {
                r.material.shader = Shader.Find("Standard");
            }
        }
        
        if (this.currentAccs.GetComponent<Transform>().childCount > 0)
        {
            MeshRenderer[] rendAccs;
            rendAccs = this.currentAccs.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer r in rendAccs)
            {
                r.material.shader = Shader.Find("Standard");
            }
        }
        

      
        if (asset.Contains("CC" + edge + ".prefab")) {
            this.currentSinkTopMesh = null;
        } else {

            /*this.currentSinkTopMesh = this.currentSinkTop.GetComponentInChildren<MeshFilter>().mesh;
            Material countertopmaterial = (Material)Resources.Load("Materials/Countertop");
            this.currentSinkTop.GetComponentInChildren<MeshRenderer>().material = countertopmaterial;*/

            if (this.currentSinkTop.GetComponent<Transform>().childCount > 0)
            {
                MeshFilter[] rendSinkTopMesh;
                rendSinkTopMesh = this.currentSinkTop.GetComponentsInChildren<MeshFilter>();
                this.currentSinkTopMesh = rendSinkTopMesh[0].mesh;
                Material countertopmaterial = (Material)Resources.Load("Materials/Countertop");

                 if (this.currentSinkTop.GetComponent<Transform>().childCount > 0)
                {
                    MeshRenderer[] rendSinkTop;
                    rendSinkTop = this.currentSinkTop.GetComponentsInChildren<MeshRenderer>();
                    foreach(MeshRenderer r in rendSinkTop)
                    {
                        r.material = countertopmaterial;
                    }
                }
            }
        }
        this.currentSink.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        this.currentSinkTop.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        this.currentDepthMask.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        this.currentAccs.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);

        this.currentSink.transform.parent =
		this.currentSinkTop.transform.parent =
		this.currentDepthMask.transform.parent =
		this.currentAccs.transform.parent = this.root;
        //this.root.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
      

        this.currentSink.transform.localPosition = 
		this.currentSinkTop.transform.localPosition =
		this.currentDepthMask.transform.localPosition =
		this.currentAccs.transform.localPosition = new Vector3(0.0f, 0.0f, offset);

        this.currentSink.transform.localRotation =
		this.currentSinkTop.transform.localRotation =
		this.currentDepthMask.transform.localRotation =
		this.currentAccs.transform.localRotation = Quaternion.identity;

        Renderer[] materials = this.currentSink.GetComponentsInChildren<Renderer>();

        foreach (Renderer material in materials) {
            material.material.mainTexture.filterMode = FilterMode.Point;
        }

        MoveVertColumn(this.currentSinkTopMesh, true, 19.0f, this.leftOffset);
        MoveVertColumn(this.currentSinkTopMesh, false, -19.0f, this.rightOffset);

        this.syncTracking();
        this.SetSinkActive(true);

        Resources.UnloadUnusedAssets();
    }

    public void DisplayCountertopEdge(string edge, AssetBundle asset)
    {
        float offset = 7.5f;

        Destroy(this.currentCountertop);
        Destroy(this.currentLeftL);
        Destroy(this.currentRightL);

        this.currentCountertop = this.currentLeftL = this.currentRightL = null;

        if (asset == null) {
            Resources.UnloadUnusedAssets();
            return;
        }

        this.currentLeftL = (GameObject)Instantiate(Resources.Load("Prefabs/Countertops/" + "C." + edge + ".L"), new Vector3(this.leftOffset, 0.0f, offset), Quaternion.identity);
        this.currentRightL = (GameObject)Instantiate(Resources.Load("Prefabs/Countertops/" + "C." + edge + ".R"), new Vector3(this.rightOffset, 0.0f, offset), Quaternion.identity);
        this.currentLeftL.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        this.currentRightL.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        if (asset.name == "s119") {
            this.currentCountertop = (GameObject)Instantiate(Resources.Load("Prefabs/911/" + "911.C." + edge), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
            this.currentCountertop.GetComponent<Transform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
            //this.currentCountertop = (GameObject)Instantiate(asset.LoadAsset("CC." + edge + ".prefab"), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
            //this.currentCountertop.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
            Material countertopmaterial = (Material)Resources.Load("Materials/Countertop");
            this.currentCountertop.GetComponentInChildren<MeshRenderer>().material = countertopmaterial;
        } else {
            this.currentCountertop = (GameObject)Instantiate(Resources.Load("Prefabs/Countertops/" + "C." + edge), new Vector3(0.0f, 0.0f, offset), Quaternion.identity);
            this.currentCountertop.GetComponent<Transform>().localScale = new Vector3(0.5f,0.5f,0.5f);
        }

        this.currentCountertopMesh = this.currentCountertop.GetComponentInChildren<MeshFilter>().mesh;
        this.currentLeftLMesh = this.currentLeftL.GetComponentInChildren<MeshFilter>().mesh;
        this.currentRightLMesh = this.currentRightL.GetComponentInChildren<MeshFilter>().mesh;

        this.currentCountertop.transform.parent =
		this.currentLeftL.transform.parent =
		this.currentRightL.transform.parent = this.root;

        this.currentCountertop.transform.localPosition = new Vector3(0.0f, 0.0f, offset);
        this.currentLeftL.transform.localPosition = new Vector3(this.leftOffset, 0.0f, offset);
        this.currentRightL.transform.localPosition = new Vector3(this.rightOffset, 0.0f, offset);

        this.currentCountertop.transform.localRotation =
		this.currentLeftL.transform.localRotation =
		this.currentRightL.transform.localRotation = Quaternion.identity;

        this.currentLeftL.SetActive(this.isLeftDown);
        this.currentRightL.SetActive(this.isRightDown);

        MoveVertColumn(this.currentCountertopMesh, true, 19.0f, this.leftOffset);
        MoveVertColumn(this.currentCountertopMesh, false, -19.0f, this.rightOffset);
        MoveVertRow(this.currentLeftLMesh, 0.5f, this.leftDownOffset);
        MoveVertRow(this.currentRightLMesh, 0.5f, this.rightDownOffset);
		
        this.syncTracking();
        this.SetSinkActive(true);

        Resources.UnloadUnusedAssets();
    }

    public void DisplayCountertop(string category, string countertop)
    {
        Resources.UnloadAsset(this.countertopTexture);
        this.countertopTexture = null;
        Resources.UnloadUnusedAssets();

        this.countertopTexture = (Texture2D)Resources.Load("Textures/Countertops/" + category + "/" + countertop);

        if (this.countertopTexture) {
            Material mat = (Material)Resources.Load("Materials/Countertop");
            mat.mainTexture = this.countertopTexture;
        }
    }

    void syncTracking()
    {
        Renderer[] renderers = this.root.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderers.Length; ++i) {
            renderers[i].enabled = this.trackingActive;
        }
    }

    bool Left(float delta)
    {
        if (this.leftOffset + delta < 0) {
            delta = -1.0f * this.leftOffset;
        }
        if (MoveVertColumn(this.currentCountertopMesh, true, 19.0f, delta) &&
            MoveVertColumn(this.currentSinkTopMesh, true, 19.0f, delta)) {
            this.leftArrow.transform.Translate(delta/2, 0.0f, 0.0f);
            this.leftArrowDown.transform.Translate(delta/2, 0.0f, 0.0f);
            this.currentLeftL.transform.Translate(delta/2, 0.0f, 0.0f);
            this.leftOffset += delta;
            return true;
        }

        return false;
    }

    bool Right(float delta)
    {
        if (this.rightOffset + delta > 0) {
            delta = -1.0f * this.rightOffset;
        }
        if (MoveVertColumn(this.currentCountertopMesh, false, -19.0f, delta) &&
            MoveVertColumn(this.currentSinkTopMesh, false, -19.0f, delta)) {
            this.rightArrow.transform.Translate(delta/2, 0.0f, 0.0f);
            this.rightArrowDown.transform.Translate(delta/2, 0.0f, 0.0f);
            this.currentRightL.transform.Translate(delta/2, 0.0f, 0.0f);
            this.rightOffset += delta;
            return true;
        }

        return false;
    }

    bool LeftDown(float delta)
    {
        if (this.leftDownOffset + delta < 0) {
            delta = -1.0f * this.leftDownOffset;
        }
        if (MoveVertRow(this.currentLeftLMesh, 0.5f, delta)) {
            this.leftArrowDown.transform.Translate(0.0f, 0.0f, delta/2);
            this.leftDownOffset += delta/2;
            return true;
        }

        return false;
    }

    bool RightDown(float delta)
    {
        if (this.rightDownOffset + delta < 0) {
            delta = -1.0f * this.rightDownOffset;
        }
        if (MoveVertRow(this.currentRightLMesh, 0.5f, delta)) {
            this.rightArrowDown.transform.Translate(0.0f, 0.0f, delta/2);
            this.rightDownOffset += delta/2;
            return true;
        }

        return false;
    }

    bool MoveVertColumn(Mesh themesh, bool isleft, float thresholdX, float deltaX)
    {
        if (themesh == null) {
            return true;
        }

        Vector3[] verts = themesh.vertices;
        Vector2[] uvs = themesh.uv;
        bool found = false;

        for (int i = 0; i < verts.Length; i++) {
            if ((isleft && verts[i].x > thresholdX) ||
                (!isleft && verts[i].x < thresholdX)) {
                verts[i].x += deltaX;
                uvs[i].x -= deltaX / 40.0f;
                found = true;
            }
        }

        if (found) {
            themesh.vertices = verts;
            themesh.uv = uvs;
            themesh.RecalculateBounds();
            return true;
        }

        return false;
    }

    bool MoveVertRow(Mesh themesh, float thresholdY, float deltaY)
    {
        if (themesh == null) {
            return true;
        }

        Vector3[] verts = themesh.vertices;
        Vector2[] uvs = themesh.uv;
        bool found = false;

        for (int i = 0; i < verts.Length; i++) {
            if (verts[i].z > thresholdY) {
                verts[i].z += deltaY;
                uvs[i].y -= deltaY / 40.0f;
                found = true;
            }
        }

        if (found) {
            themesh.vertices = verts;
            themesh.uv = uvs;
            themesh.RecalculateBounds();
            return true;
        }

        return false;
    }

    public void UpdateTracking(bool newtracking)
    {
        //Debug.Log("Frustum::UpdateTracking: " + newtracking);
        this.trackingActive = newtracking;
    }

#region INPUT
    public bool OnTouchStart(Touch touch)
    {
        if (this.arrowsActive) {
            Plane plane = new Plane(transform.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            float dist = 0.0f;
            plane.Raycast(ray, out dist);
            Vector3 planePoint = ray.GetPoint(dist);
			
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 1000)) {
                if (this.rootID == -1 && hit.transform.name == "Root") {
                    this.rootID = touch.fingerId;
                    this.lastRootOffset = planePoint.x;
					
                    return true;
                } else if (this.leftID == -1 && hit.transform.name == "LeftArrow") {
                    this.leftID = touch.fingerId;
                    this.lastLeftOffset = planePoint.x;
                    this.isLeftTogglable = true;
					
                    return true;
                } else if (this.rightID == -1 && hit.transform.name == "RightArrow") {
                    this.rightID = touch.fingerId;
                    this.lastRightOffset = planePoint.x;
                    this.isRightTogglable = true;
					
                    return true;
                } else if (this.leftDownID == -1 && hit.transform.name == "LeftArrowDown") {
                    this.leftDownID = touch.fingerId;
                    this.lastLeftDownOffset = planePoint.z;
                    this.isLeftTogglable = true;
					
                    return true;
                } else if (this.rightDownID == -1 && hit.transform.name == "RightArrowDown") {
                    this.rightDownID = touch.fingerId;
                    this.lastRightDownOffset = planePoint.z;
                    this.isRightTogglable = true;
					
                    return true;
                }
            }
        }
		
        return false;
    }

    public void OnTouchMove(Touch touch)
    {
        Plane plane = new Plane(transform.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(touch.position);
        float dist = 0.0f;
        plane.Raycast(ray, out dist);
        Vector3 planePoint = ray.GetPoint(dist);
        if (touch.fingerId == this.rootID) {
            this.root.Translate(planePoint.x - this.lastRootOffset, 0.0f, 0.0f);
            this.lastRootOffset = planePoint.x;
        } else if (touch.fingerId == this.leftID) {
            Left(planePoint.x - this.lastLeftOffset);
            this.lastLeftOffset = planePoint.x;
            this.isLeftTogglable = false;
        } else if (touch.fingerId == this.rightID) {
            Right(planePoint.x - this.lastRightOffset);
            this.lastRightOffset = planePoint.x;
            this.isRightTogglable = false;
        } else if (touch.fingerId == this.leftDownID) {
            LeftDown(planePoint.z - this.lastLeftDownOffset);
            this.lastLeftDownOffset = planePoint.z;
            this.isLeftTogglable = false;
        } else if (touch.fingerId == this.rightDownID) {
            RightDown(planePoint.z - this.lastRightDownOffset);
            this.lastRightDownOffset = planePoint.z;
            this.isRightTogglable = false;
        }
    }

    public void OnTouchCancel(int fingerid)
    {
        if (fingerid == this.rootID) {
            this.rootID = -1;
        } else if (fingerid == this.leftID) {
            this.leftID = -1;
            this.isLeftTogglable = false;
        } else if (fingerid == this.rightID) {
            this.rightID = -1;
            this.isRightTogglable = false;
        } else if (fingerid == this.leftDownID) {
            this.leftDownID = -1;
            this.isLeftTogglable = false;
        } else if (fingerid == this.rightDownID) {
            this.rightDownID = -1;
            this.isRightTogglable = false;
        }
    }

    public void OnTouchEnd(Touch touch)
    {
        if (touch.fingerId == this.rootID) {
            this.rootID = -1;
        } else if (touch.fingerId == this.leftID) {
            this.leftID = -1;
            if (this.isLeftTogglable) {
                this.isLeftDown = true;
                this.leftArrow.gameObject.SetActive(false);
                this.leftArrowDown.gameObject.SetActive(true);
                this.currentLeftL.SetActive(true);
            }
        } else if (touch.fingerId == this.rightID) {
            this.rightID = -1;
            if (this.isRightTogglable) {
                this.isRightDown = true;
                this.rightArrow.gameObject.SetActive(false);
                this.rightArrowDown.gameObject.SetActive(true);
                this.currentRightL.SetActive(true);
            }
        } else if (touch.fingerId == this.leftDownID) {
            this.leftDownID = -1;
            if (this.isLeftTogglable) {
                this.isLeftDown = false;
                this.leftArrow.gameObject.SetActive(true);
                this.leftArrowDown.gameObject.SetActive(false);
                this.currentLeftL.SetActive(false);
            }
        } else if (touch.fingerId == this.rightDownID) {
            this.rightDownID = -1;
            if (this.isRightTogglable) {
                this.isRightDown = false;
                this.rightArrow.gameObject.SetActive(true);
                this.rightArrowDown.gameObject.SetActive(false);
                this.currentRightL.SetActive(false);
            }
        }
    }
#endregion



    Transform root, centerArrow, leftArrow, rightArrow, leftArrowDown, rightArrowDown;
    GameObject currentSink, currentSinkTop, currentCountertop, currentDepthMask, currentAccs, currentLeftL, currentRightL;
    Texture2D countertopTexture;
    Mesh currentSinkTopMesh, currentCountertopMesh, currentLeftLMesh, currentRightLMesh;

    int rootID, leftID, rightID, leftDownID, rightDownID;
    float leftOffset, rightOffset, leftDownOffset, rightDownOffset;
    float lastRootOffset;
    float lastLeftOffset, lastRightOffset, lastLeftDownOffset, lastRightDownOffset;
    bool isLeftDown, isRightDown, isLeftTogglable, isRightTogglable;
    bool trackingActive, sinkActive;
    bool arrowsActive;
}
