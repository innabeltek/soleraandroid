﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.ComponentModel;
using UnityEngine.UI;

public class Bundle 
{
    public AssetBundle Asset { get { return this.asset; } }

    public bool Loading { get { return this.loading; } }
    public bool Loaded { get { return this.loaded; } }
    public bool Downloading { get { return this.downloading; } }
    public bool Downloaded { get { return this.downloaded; } }

    public event Action LoadStartEvent;
    public event Action LoadEndEvent;
    public event Action DownloadStartEvent;
    public event Action<int> DownloadProgressEvent;
    public event Action DownloadEndEvent;

    public string localPath;

    public Text text;

    public Bundle(string basePath)
    {
        this.basePath = basePath;

        this.loaded = this.loading = false;
        this.downloaded = this.downloading = false;
    }


    public void ClearEvents()
    {
        LoadStartEvent = null;
        LoadEndEvent = null;
        DownloadStartEvent = null;
        DownloadProgressEvent = null;
        DownloadEndEvent = null;
    }




    public IEnumerator Load(string path)
    {
        localPath = this.basePath + "/" + path;

        Debug.Log("Load: localPath: " + localPath);

        this.Unload();

        if (!File.Exists(localPath)) {
            IEnumerator e = this.Download(path);
            Debug.Log("New Sink Loaded");
            while (e.MoveNext()) {
                yield return e.Current;
            }
        }

        if (File.Exists(localPath)) {
            this.loading = true;
            if (LoadStartEvent != null) {
                LoadStartEvent();
            }

            AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(localPath);
            yield return request;

                 this.asset = request.assetBundle;
            if (this.asset == null) {
                Debug.Log("Failed to load Asset Bundle: " + localPath);
                this.loaded = false;
            } else {
                Debug.Log("Loaded Asset Bundle: " + localPath);
                this.loaded = true;
            }

            if (LoadEndEvent != null) {
                LoadEndEvent();
            }
            this.loading = false;
        }

        Resources.UnloadUnusedAssets();
    }

    public void Unload(bool complete = true)
    {
        if (this.asset != null) {
            this.asset.Unload(complete);
            this.asset = null;
        }

        this.loaded = false;
    }


    public IEnumerator Download(string path)
    {
        string remotePath;
        this.downloaded = false;
        this.downloading = true;
        /* if (path == "sinklist")
         {
             remotePath = "https://www.dropbox.com/s/gg8qqujsvelgqza/sinklist?dl=1";
         }
         else
         {*/
        remotePath = "https://www.solerasinks.com/files/soleravision/545/android/" + path;
        //}
        
        this.downloadPath = this.basePath + "/" + path;
       
        WWW www = WWW.LoadFromCacheOrDownload(remotePath, 0);
        //WWW www = new WWW(remotePath);
        yield return www;

        Debug.Log("Downloading " + this.downloadPath + " from " + remotePath + path);


        if (www.error != null)
            throw new Exception("WWW download had an error:" + www.error);
        else
        {
            this.downloading = false;
            this.downloaded = true;
            Debug.Log("Download successful!");
        }

        //AssetBundleCreateRequest request = AssetBundle.LoadAsync(remotePath);
        //yield return request;
        this.asset = www.assetBundle;
        Resources.UnloadUnusedAssets();


        /*string remotePath;
        int downloadPercent;

        this.downloadPath = this.basePath + "/" + path;
#if UNITY_IOS
        remotePath = "https://www.solerasinks.com/files/soleravision/545/ios/" + path;
        //remotePath = "http://solera.dev.mrd.local/files/soleravision/545/ios/" + path;
#elif UNITY_ANDROID || UNITY_EDITOR
        remotePath = "https://www.solerasinks.com/files/soleravision/545/android/" + path;
        //remotePath = "http://solera.dev.mrd.local/files/soleravision/545/android/" + path;
#endif

        Debug.Log("Downloading " + this.downloadPath + " from " + remotePath);

        this.downloaded = false;
        this.downloading = true;

        Uri uri = new Uri(remotePath);
        if (this.client == null) {
            this.client = new CustomWebClient();

            this.client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressed);
            this.client.DownloadDataCompleted += new DownloadDataCompletedEventHandler(DownloadCompleted);
        }

        this.downloading = true;
        if (DownloadStartEvent != null) {
            DownloadStartEvent();
        }

        downloadPercent = this.downloadPercentage = 0;

        this.client.DownloadDataAsync(uri);

        while (this.downloading) {
            if (downloadPercent != this.downloadPercentage) {
                downloadPercent = this.downloadPercentage;
                if (DownloadProgressEvent != null) {
                    DownloadProgressEvent(downloadPercent);
                }
            }

            yield return new WaitForEndOfFrame();
        }

        if (this.downloaded) {
            Debug.Log("Download successful!");
#if UNITY_IPHONE
            UnityEngine.iOS.Device.SetNoBackupFlag(this.downloadPath);
#endif
        }

        if (DownloadEndEvent != null) {
            DownloadEndEvent();
        }

        Resources.UnloadUnusedAssets();*/
    }



    protected void DownloadProgressed(object sender, DownloadProgressChangedEventArgs e)
    {
        Debug.Log("DownloadProgressed: " + e.ProgressPercentage + " " + e.BytesReceived);
        this.downloadPercentage = e.ProgressPercentage;
    }

    protected void DownloadCompleted(object sender, DownloadDataCompletedEventArgs e)
    {
        this.downloading = false;

        if (!e.Cancelled && e.Error == null) {
            this.downloaded = true;
            byte[] data = (byte[])e.Result;

            FileStream cache = new FileStream(this.downloadPath, FileMode.Create);
            cache.Write(data, 0, data.Length);
            cache.Close();
            cache.Dispose();
            cache = null;
        } else {
            this.downloaded = false;
        }
    }



    AssetBundle asset;

    bool loading;
    bool loaded;
    bool downloading;
    bool downloaded;


    string basePath, downloadPath;
    int downloadPercentage;

    CustomWebClient client;
}