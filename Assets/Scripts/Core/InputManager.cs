﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager
{
    public void Initialize()
    {
        this.inputList = new List<IInputtable>(5);
        this.deleteList = new List<IInputtable>(2);
		
        this.inputMap = new Dictionary<int, IInputtable>(5);
    }

    public void Register(IInputtable input)
    {
        this.inputList.Add(input);

        this.ClearMap();
    }

    public void Deregister(IInputtable input)
    {
        if (this.inputList.Contains(input)) {
            this.deleteList.Add(input);
        }
    }

    public void Process()
    {
        for (int i = 0; i < Input.touches.Length; ++i) {
            Touch touch = Input.touches[i];

            this.DeleteInputs();
            switch (touch.phase) {
                case TouchPhase.Began:
                    this.CancelMapping(touch.fingerId);
                    for (int j = this.inputList.Count - 1; j >= 0; --j) {
                        if (this.inputList[j].OnTouchStart(touch)) {
                            this.inputMap.Add(touch.fingerId, this.inputList[j]);
                            break;
                        }
                    }
                    break;
                case TouchPhase.Moved:
                    if (touch.deltaTime > 0.0f && this.inputMap.ContainsKey(touch.fingerId)) {
                        this.inputMap[touch.fingerId].OnTouchMove(touch);
                    }
                    break;
                case TouchPhase.Canceled:
                    this.CancelMapping(touch.fingerId);
                    break;
                case TouchPhase.Ended:
                    if (this.inputMap.ContainsKey(touch.fingerId)) {
                        this.inputMap[touch.fingerId].OnTouchEnd(touch);
                        this.inputMap.Remove(touch.fingerId);
                    }
                    break;
            }
        }
    }

    void DeleteInputs()
    {
        if (this.deleteList.Count > 0) {
            List<int> keyList = new List<int>(this.inputMap.Count);
            for (int i = 0; i < this.deleteList.Count; ++i) {
                this.inputList.Remove(this.deleteList[i]);
                foreach (KeyValuePair<int, IInputtable> input in this.inputMap) {
                    if (input.Value == this.deleteList[i]) {
                        keyList.Add(input.Key);
                    }
                }
                for (int j = 0; j < keyList.Count; ++j) {
                    this.CancelMapping(keyList[j]);
                }
            }
            this.deleteList.Clear();
        }
    }

    void ClearMap()
    {
        foreach (KeyValuePair<int, IInputtable> input in this.inputMap) {
            input.Value.OnTouchCancel(input.Key);
        }

        this.inputMap.Clear();
    }

    void CancelMapping(int id)
    {
        if (this.inputMap.ContainsKey(id)) {
            this.inputMap[id].OnTouchCancel(id);
            this.inputMap.Remove(id);
        }
    }

    List<IInputtable> inputList, deleteList;
    Dictionary<int, IInputtable> inputMap;
}

public interface IInputtable
{
    bool OnTouchStart(Touch touch);

    void OnTouchMove(Touch touch);

    void OnTouchCancel(int fingerid);

    void OnTouchEnd(Touch touch);
}
