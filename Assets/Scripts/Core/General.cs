using UnityEngine;
using System;



public static class MyExtensions
{
    public static int Clamp(this int integer, int floor, int ceiling)
    {
        if (ceiling < floor) {
            return integer < ceiling ? ceiling : integer > floor ? floor : integer;
        } else {
            return integer < floor ? floor : integer > ceiling ? ceiling : integer;
        }
    }

    public static int Wrap(this int integer, int size)
    {
        return (integer % size + size) % size;
    }
}

public struct Bucket
{
    public Transform Arm, Item, SelectedUnderlay, SubheaderUnderlay, Subheader, InfoButton;
    public tk2dSprite ItemSprite;
    public tk2dTextMesh SubheaderText;
}

public class SinkInfo
{
    public string Category, Model, Title, Description, Url;
    public bool HasCustomCountertops;
    public Texture2D InfoTexture;
}

public class CountertopInfo
{
    public string Category, Description;
}

public class ItemList
{
    public ItemList(string title, int length, int breakpoints)
    {
        length = Math.Max(0, length);
        breakpoints = Math.Max(0, breakpoints);

        Title = title;
        Length = length;

        Items = new Item[length];
        Breakpoints = new Breakpoint[breakpoints];
    }

    public int Find(string item)
    {
        for (int i = 0; i < Items.Length; ++i) {
            if (Items[i].Name == item) {
                return i;
            }
        }

        return -1;
    }

    public bool IsBreakpoint(int index)
    {
        for (int i = 0; i < Breakpoints.Length; ++i) {
            if (Breakpoints[i].Index == index) {
                return true;
            }
        }

        return false;
    }

    public string GetTitle(int index, int offset = 0)
    {
        for (int i = Breakpoints.Length - 1; i >= 0; --i) {
            if (Breakpoints[i].Index <= index) {
                return Breakpoints[(i + offset).Wrap(Breakpoints.Length)].Title;
            }
        }

        return "Not Found";
    }

    public string Title;
    public int Length;

    public Item[] Items;
    public Breakpoint[] Breakpoints;
}

public struct Item
{
    public string Name;
    public bool HasCountertop, IsPresent;
}

public struct Breakpoint
{
    public int Index;
    public string Title;
}
