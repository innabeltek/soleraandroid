﻿using System;
using System.Net;

public class CustomWebClient : WebClient
{
    protected override WebRequest GetWebRequest(Uri uri)
    {
        WebRequest webRequest = base.GetWebRequest(uri);

        webRequest.Timeout = 15000;

        return webRequest;
    }
}
