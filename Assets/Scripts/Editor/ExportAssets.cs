using UnityEngine;
using UnityEditor;

using System.IO;

public class ExportAssets
{
    /*
    [MenuItem("Assets/Build AssetBundle From Selection - Track dependencies (iOS)")]
	static void ExportResourceIOS()
	{
		string path = EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
		if( path.Length != 0 )
		{
			Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
			Object mainAsset = null;
			foreach( Object obj in selection )
			{
				if( obj.name == "SinkList" || obj.name == "DefaultSinkList" )
				{
					mainAsset = obj;
				}
			}
			BuildPipeline.BuildAssetBundle
			(
					mainAsset,
					selection,
					path,
					BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets,
					BuildTarget.iPhone
			);
			Selection.objects = selection;
		}
	}

	[MenuItem("Assets/Build AssetBundle From Selection - Track dependencies (Android)")]
	static void ExportResourceAndroid()
	{
		string path = EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
		if( path.Length != 0 )
		{
			Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
			Object mainAsset = null;
			foreach( Object obj in selection )
			{
				if( obj.name == "SinkList" || obj.name == "DefaultSinkList" )
				{
					mainAsset = obj;
				}
			}
			BuildPipeline.BuildAssetBundle
			(
					mainAsset,
					selection,
					path,
					BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets,
					BuildTarget.Android
			);
			Selection.objects = selection;
		}
	}
 */   
}

public class ClearPlayerPrefs
{
	[MenuItem("File/Clear Player Preferences")]
	static void ClearPrefs()
	{
		PlayerPrefs.DeleteAll();
	}
}