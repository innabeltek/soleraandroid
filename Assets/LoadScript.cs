﻿using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadScript : MonoBehaviour
{

    void Start()
    {
        StartCoroutine(ExtractObbDatasets());
    }

    private IEnumerator ExtractObbDatasets()
    {
        string[] filesInOBB = { "Solera_Vision.dat", "Solera_Vision.xml", "unity.txt" };
        foreach (var filename in filesInOBB)
        {
            string uri = Application.streamingAssetsPath + "/QCAR/" + filename;

            string outputFilePath = Application.persistentDataPath + "/QCAR/" + filename;
            if (!Directory.Exists(Path.GetDirectoryName(outputFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(outputFilePath));

            var www = new WWW(uri);
            yield return www;

            Save(www, outputFilePath);
            yield return new WaitForEndOfFrame();
        }

        // When done extracting the datasets, Start Vuforia AR scene
        SceneManager.LoadScene("MainScene");
    }

    private void Save(WWW www, string outputPath)
    {
        File.WriteAllBytes(outputPath, www.bytes);

        // Verify that the File has been actually stored
        if (File.Exists(outputPath))
        {
            Debug.Log("File successfully saved at: " + outputPath);
        }
        else
        {
            Debug.Log("Failure!! - File does not exist at: " + outputPath);
        }
    }
}